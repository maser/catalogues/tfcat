from shapely.geometry import MultiLineString, Polygon # LineString, Polygon
from shapely.ops import split
from shapely.affinity import affine_transform
from matplotlib import pyplot as plt
from matplotlib import rcParams


# Geometry functions used to select, split and translate Phase-CML segments

# The Phase-Phase segments extracted from the catalogue metadata are not ready for plotting.
# They must be split and translated back into the [0,360] interval on each dimension


def _move(shape, x,y):
    """Move (translation) a shape (Shapely object) with offsets x and y
    """
    return affine_transform(shape, [1, 0, 0, 1, x, y])


def move_up(shape):
    """Move up (y+360)
    """
    return _move(shape, 0, 360)


def move_down(shape):
    """Move up (y-360)
    """
    return _move(shape, 0, -360)


def move_left(shape):
    """Move left (x-360)
    """
    return _move(shape, -360, 0)


def move_right(shape):
    """Move right (x+360)
    """
    return _move(shape, 360, 0)


# Define boxes for shape location checks
_mainbox = Polygon([(0, 0), (360, 0), (360, 360), (0, 360), (0, 0)])
_leftbox = move_left(_mainbox)
_rightbox = move_right(_mainbox)
_bottombox = move_down(_mainbox)
_topbox = move_up(_mainbox)
_toprightbox = move_right(move_up(_mainbox))
_bottomrightbox = move_right(move_down(_mainbox))
_topleftbox = move_left(move_up(_mainbox))
_bottomleftbox = move_left(move_down(_mainbox))


def plot_slopes(lines):
    """Plots the slopes of a list of lines
    """
    slopes = []
    for line in lines:
        slopes.append(slope(line))
    plt.plot(slopes, '+')
    plt.show()


def debug_plot(line_tuple):
    for lines, color, title in line_tuple:
        rcParams.update({'font.size': 14})
        plt.figure(figsize=(20, 10))

        plt.plot(*_mainbox.exterior.xy, 'r')
        plt.plot(*_leftbox.exterior.xy, 'y')
        plt.plot(*_rightbox.exterior.xy, 'y')
        plt.plot(*_bottombox.exterior.xy, 'y')
        plt.plot(*_topbox.exterior.xy, 'y')

        for line in lines:
            plt.plot(*line.xy, color)
        plt.title(title)
        plt.show()
        plot_slopes(lines)
        plt.show()


def slope(line):
    """Computes the slope of a line
    """
    x,y = line.xy
    return (y[1]-y[0])/(x[1]-x[0])


def _geometry_processing_step1(lines, debug=False):

    # STEP 1: select all lines in mainbox, and put the rest in tmp0
    start_clean = []
    other_lines = []
    for line in lines:
        if line.within(_mainbox):
            start_clean.append(line)
        else:
            other_lines.append(line)

    if debug:
        debug_plot([
            (start_clean, 'k', "Step 1 - clean"),
            (other_lines, 'r', "Step 1 - other lines")
        ])

    return start_clean, other_lines


def _geometry_processing_step2(lines, debug=False):

    # STEP 2: move lines from side boxes into main box, and put the rest in tmp1
    move_clean = []
    tmp1 = []
    for line in lines:
        if line.within(_leftbox):
            move_clean.append(move_right(line))
        elif line.within(_rightbox):
            move_clean.append(move_left(line))
        elif line.within(_bottombox):
            move_clean.append(move_up(line))
        elif line.within(_topbox):
            move_clean.append(move_down(line))
        else:
            tmp1.append(line)

    if debug:
        debug_plot([
            (move_clean, 'k', "Step 2 - moved"),
            (tmp1, 'r', "Step 2 - other lines")
        ])

    return move_clean, tmp1


def _geometry_processing_step3(lines, debug=False):

    # STEP 3: split lines intersecting main box sides
    tmp2 = []
    for line in lines:
        if line.intersects(_mainbox.intersection(_topbox)):
            tmp2.extend(split(line, _mainbox.intersection(_topbox)).geoms)
        elif line.intersects(_mainbox.intersection(_rightbox)):
            tmp2.extend(split(line, _mainbox.intersection(_rightbox)).geoms)
        elif line.intersects(_mainbox.intersection(_bottombox)):
            tmp2.extend(split(line, _mainbox.intersection(_bottombox)).geoms)
        elif line.intersects(_mainbox.intersection(_leftbox)):
            tmp2.extend(split(line, _mainbox.intersection(_leftbox)).geoms)
        else:
            tmp2.append(line)

    split_in = []
    split_out = []
    tmp3 = []

    for line in tmp2:
        if line.within(_mainbox):
            split_in.append(line)
        elif line.within(_leftbox):
            split_out.append(move_right(line))
        elif line.within(_rightbox):
            split_out.append(move_left(line))
        elif line.within(_bottombox):
            split_out.append(move_up(line))
        elif line.within(_topbox):
            split_out.append(move_down(line))
        else:
            tmp3.append(line)

    if debug:
        debug_plot([
            (split_in, 'k', "Step 3 - split (internal part)"),
            (split_out, 'k', "Step 3 - split (external part, moved in)"),
            (tmp3, 'k', "Step 3 - other lines)"),
        ])

    return split_in, split_out, tmp3


def _geometry_processing_step4(lines, debug=False):

    # STEP 4: move lines intersecting side boxes sides, and put the rest in tmp4
    moved = []
    tmp4 = []
    for line in lines:
        if line.intersects(
            _topbox.intersection(_toprightbox)
        ) or line.intersects(
            _topbox.intersection(_topleftbox)
        ):
            moved.append(move_down(line))
        elif line.intersects(
            _bottombox.intersection(_bottomrightbox)
        ) or line.intersects(
            _bottombox.intersection(_bottomleftbox)
        ):
            moved.append(move_up(line))
        elif line.intersects(
            _rightbox.intersection(_toprightbox)
        ) or line.intersects(
            _rightbox.intersection(_bottomrightbox)
        ):
            moved.append(move_left(line))
        elif line.intersects(
            _leftbox.intersection(_topleftbox)
        ) or line.intersects(
            _leftbox.intersection(_bottomleftbox)
        ):
            moved.append(move_right(line))
        else:
            tmp4.append(line)

    if debug:
        debug_plot([
            (moved, 'k', "Step 4 - moved (internal part)"),
            (tmp4, 'k', "Step 4 - other lines)"),
        ])

    return moved, tmp4


def _geometry_processing_step5(lines, debug=False):

    # STEP 5: split lines intersecting main boxes sides (again),
    # and put outside chunks in split_moved
    tmp5 = []
    for line in lines:
        if line.intersects(_mainbox.intersection(_topbox)):
            tmp5.extend(split(line, _mainbox.intersection(_topbox)).geoms)
        elif line.intersects(_mainbox.intersection(_rightbox)):
            tmp5.extend(split(line, _mainbox.intersection(_rightbox)).geoms)
        elif line.intersects(_mainbox.intersection(_bottombox)):
            tmp5.extend(split(line, _mainbox.intersection(_bottombox)).geoms)
        elif line.intersects(_mainbox.intersection(_leftbox)):
            tmp5.extend(split(line, _mainbox.intersection(_leftbox)).geoms)
        else:
            tmp5.append(line)

    split_in2 = []
    split_out2 = []
    tmp6 = []

    for line in tmp5:
        if line.within(_mainbox):
            split_in2.append(line)
        elif line.within(_leftbox):
            split_out2.append(move_right(line))
        elif line.within(_rightbox):
            split_out2.append(move_left(line))
        elif line.within(_bottombox):
            split_out2.append(move_up(line))
        elif line.within(_topbox):
            split_out2.append(move_down(line))
        else:
            tmp6.append(line)

    if debug:
        debug_plot([
            (split_in2, 'k', "Step 5 - split (internal part)"),
            (split_out2, 'k', "Step 5 - split (external part, moved in)"),
            (tmp6, 'k', "Step 5 - other lines)"),
        ])

    return split_in2, split_out2, tmp6


def full_processing(lines, debug=False):

    # STEP 1: select all lines in mainbox, and put the rest in tmp0
    start_clean, tmp1 = _geometry_processing_step1(lines, debug)

    # STEP 2: move lines from side boxes into main box, and put the rest in tmp1
    move_clean, tmp2 = _geometry_processing_step2(tmp1, debug)

    # STEP 3: split lines intersecting main box sides
    split_in, split_out, tmp3 = _geometry_processing_step3(tmp2, debug)

    # STEP 4: move lines intersecting side boxes sides, and put the rest in tmp4
    moved, tmp4 = _geometry_processing_step4(tmp3, debug)

    # STEP 5: split lines intersecting main boxes sides (again),
    # and put outside chunks in split_moved
    split_in2, split_out2, tmp5 = _geometry_processing_step5(tmp4, debug)

    # Final plot:

    return start_clean + move_clean + split_in + split_out + split_in2 + split_out2


def histogram(lines):
    import numpy

    dx = 5
    xhisto = numpy.arange(0, 720, dx)
    cmls = []
    phases = []

    for line in lines:
        cml, phase = line.xy
        cmls.extend(xhisto[(xhisto >= cml[0]) & ((xhisto - dx) <= cml[1])] % 360)
        phases.extend(xhisto[(xhisto >= phase[0]) & ((xhisto - dx) <= phase[1])] % 360)

    y_cml, x_cml = numpy.histogram(cmls, bins=72, range=[0,360])
    y_phase, x_phase = numpy.histogram(phases, bins=72, range=[0,360])

    return y_cml, x_cml, y_phase, x_phase


def plot_lines(lines, ax, xlabel=None, ylabel=None, title=None, debug=False, **kwargs):
    """Plotting phase diagram (usually Io-CML)
    """

    final_lines = full_processing(lines, debug=debug)

    for line in final_lines:
        ax.plot(*line.xy, 'k', **kwargs)

    ax.set_xlim((0, 360))
    ax.set_ylim((0, 360))
    ax.set_box_aspect(1)
    ax.set_xticks([i * 60 for i in range(7)])
    ax.set_yticks([i * 60 for i in range(7)])
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)

    return final_lines


def plot_phase_cml(lines, xlabel=None, ylabel=None, title=None, png_file=None, debug=False, **kwargs):

    fig, ax = plt.subplots(figsize=(15, 15))
    rcParams.update({'font.size': 22})

    _ = plot_lines(lines, ax, xlabel=xlabel, ylabel=ylabel, title=title, debug=debug, **kwargs)

    if png_file is not None:
        plt.savefig(png_file)
        plt.close(fig)
    else:
        plt.show()


def plot_histogram(lines, xlabel=None, ylabel=None, title=None, png_file=None, axs=None, debug=False):
    """Plotting histogram (against Io-Phase or CML)
    """

    y_cml, x_cml, y_phase, x_phase = histogram(lines)

    axs_is_set = False if axs is None else True

    if not axs_is_set:
        fig, axs = plt.subplots(2, 1, figsize=(20,20))

    axs[0].hist(x_cml[:-1], x_cml, weights=y_cml)
    axs[0].set_title('histogram of CMLs')
    axs[0].set_xlabel('CML (deg)')
    axs[0].set_xlim((0, 360))
    axs[0].set_ylabel("Count")
    axs[1].step(x_phase[:-1], x_phase, weights=y_phase)
    axs[1].set_title('Histogram of Io-Phases')
    axs[1].set_xlabel('CML (deg)')
    axs[1].set_xlim((0, 360))
    axs[1].set_ylabel("Count")


def full_plot(all_lines: list, png_file=None, debug=False):
    fig, axs = plt.subplots(2, 2,
        figsize=(20, 20),
        sharex="col",
        sharey="row",
        gridspec_kw=dict(
            height_ratios=[1, 3],
            width_ratios=[3, 1])
    )

    axs[0, 1].set_visible(False)
    axs[0, 0].set_box_aspect(1 / 3)
    axs[1, 0].set_box_aspect(1)
    axs[1, 1].set_box_aspect(3 / 1)

    plot_lines(all_lines, ax=axs[1, 0])
    plot_histogram(all_lines, axs=[axs[0, 0], axs[1, 1]])

    plt.show()