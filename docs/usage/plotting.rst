Plotting TFcat Objects
======================

Built-in plotting
-----------------

The TFCat library includes a simple plotting interface at feature
level.

.. code-block:: python
   :caption: Example 1: Plotting a Multipoint feature

   from tfcat import TFCat
   tf_url = 'https://maser.obspm.fr/doi/10.25935/r11g-6j63/content/cassini_faraday_patches_2006.json'
   cat = TFCat.from_url(tf_url)
   cat.plot_feature(0)

.. image:: img/example1.png
  :width: 400
  :alt: Example 1 Plot

.. code-block:: python
   :caption: Example 2: Plotting a Polygon feature

   from tfcat import TFCat
   tf_url = 'https://maser.obspm.fr/doi/10.25935/hfjx-xx26/content/fogg_akr_burst_list_2000.json'
   cat = TFCat.from_url(tf_url)
   cat.plot_feature(100)

.. image:: img/example2.png
  :width: 400
  :alt: Example 2 Plot

.. code-block:: python
   :caption: Example 3: Plotting a MultiPolygon feature

   from tfcat import TFCat
   tf_url = 'https://maser.obspm.fr/doi/10.25935/t033-qs72/content/data/wu_2022_catalogue.json'
   cat = TFCat.from_url(tf_url)
   cat.plot_feature(0)

.. image:: img/example3.png
  :width: 400
  :alt: Example 3 Plot

Advanced plotting
-----------------

After transforming feature geometries into shapely objects (see :doc:`processing section <processing>`),
all examples presented in the `shapely user guide <https://shapely.readthedocs.io/en/stable/manual.html>`_
can be applied.
