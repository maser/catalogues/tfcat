Processing TFCat Objects
========================

TFCat objects have been designed out of the GeoJSON specification, replacing
the geographic coordinates with temporal-spectral coordinates. So apart from
the CRS (Coordinate Reference System), the content of the TFCat JSON files
are compliant with the GeoJSON specification. Geometry management tools
applicable to GeoJSON geometries can thus be used, such as `shapely
<https://shapely.readthedocs.io/en/stable/>`_, as shown below.

Using *shapely*
---------------
The *shapely* library offers a very well developed set of tools for managing
geometries, including transformations, conversions, operations, etc.

In the small code example below, we load a catalogue containing *MultiPoint*
features, and compute the convex hull surrounding the feature.

.. code-block:: python
   :caption: Example 1: Convex Hull

   from tfcat import TFCat
   from shapely.geometry import shape
   tf_url = 'https://maser.obspm.fr/doi/10.25935/r11g-6j63/content/cassini_faraday_patches_2006.json'
   cat = TFCat.from_url(tf_url)
   event = shape(cat.feature(0).geometry)
   hull = event.convex_hull
   from matplotlib import pyplot as plt
   plt.plot(*list(zip(*[item.coords.xy for item in event.geoms])), '+')
   plt.plot(*hull.exterior.xy, '--')
   plt.xlabel(cat.crs.time_label)
   plt.ylabel(cat.crs.spectral_label)
   plt.show()

.. image:: img/example4.png
  :width: 400
  :alt: Example 1 Plot

More examples
-------------

Examples are provides on `Jupyter notebooks <https://doi.org/10.25935/qwpa-7t05>`_. Links to
executable notebooks are provided therein for testing purposes.
