.. TFCat documentation master file, created by
   sphinx-quickstart on Thu Sep 15 09:40:45 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to TFCat's documentation!
=================================

The TFCat (Time-Frequency Catalogue) library is a utility to create and manage
TFCat objects following the `TFCat specification <https://gitlab.obspm.fr/maser/catalogues/catalogue-format>`_.

TFCat model is structured as follows:

* *TFCat Geometry* --- There are 7 types of TFCat Geometry: *Point*, *MultiPoint*,
  *LineString*, *MultiLineString*, *Polygon*, *MultiPolygon*, with an additional
  *GeometryCollection*, which is a set of any of the six other geometry types.
  Each geometry is composed of a set of coordinate pairs. A *LineString* is defined
  by a list of connected *Point* features, forming a continuous path. A *Polygon*
  is defined by a closed list of connected *Point* features, forming a closed
  contour. The feature type definitions are inspired from the `GeoJSON specification
  <https://doi.org/10.17487/RFC7946>`_.
* *TFCat Feature* --- A TFCat Feature contains one or several TFCat Geometries, and
  a set of properties, provided as a set of (key, value) pairs.
* *TFCat Field* --- A TFCat Field is defining the TFCat feature properties. Its
  minimal content includes a name, a description, a data type and a `UCD (Unified
  Content Descriptors) <https://doi.org/10.5479/ADS/bib/2021ivoa.spec.0616C>`_. It may
  also include a unit (following the `VOUnit --Virtual Observatory Unit-- specification
  <https://doi.org/10.5479/ADS/bib/2014ivoa.spec.0523D>`_), or a set of allowed values,
  when applicable.
* *TFCat Property* --- A TFCat Property is a (key, value) pair. When used  in a TFCat
  feature, the properties must be defined as a TFCat Field. When used at the main TFCat
  level, it should provide generic information on the catalogue, such as: title,
  authors, reference, instrument, etc.
* *TFCat Coordinate Reference System* --- A TFCat Coordination Reference System (CRS)
  contains a description of the time axis, a description of the spectral axis, and a
  reference position. The TFCat CRS could be of three types: *name*, *link* or *local*.
  Note that the *local* type is the only one supported in this version.


Current version and namespace
-----------------------------

The current TFCat supported specification version is 1.0. The corresponding TFCat JSON Schema URI is:
`https://voparis-ns.obspm.fr/maser/tfcat/v1.0/schema# <https://voparis-ns.obspm.fr/maser/tfcat/v1.0/schema#>`_


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   usage/creating
   usage/plotting
   usage/processing
   api/modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
