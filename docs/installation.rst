Installation
============

TFCat is available from PyPI (https://pypi.org/project/TFCat/).
The installation is thus done using the pip utility:

.. code-block::

       pip install tfcat

Requirements
------------

TFCat has been tested on Python 3.7, and requires the following
dependencies:

* astropy
* matplotlib
* shapely
* jsonschema
