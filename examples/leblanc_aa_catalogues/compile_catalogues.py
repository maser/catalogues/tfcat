#!/usr/env/bin python
"""
This script produces compile TFCat JSON files from the Leblanc et al., A&AS catalogues (1981 to 1993)
"""

__author__ = 'Baptiste Cecconi'
__copyright__ = 'Copyright 2021, Baptiste Cecconi, PADC/MASER'
__credits__ = ['Baptiste Cecconi']
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = 'Baptiste Cecconi'
__email__ = 'baptiste.cecconi@obspm.fr'
__status__ = "development"

from pathlib import Path
from tfcat import TFCat, Feature, FeatureCollection, dump
import os
from astropy.time import Time
import numpy
from jupiter_ephem import get_cml, get_jovian_moon_phase, get_jupiter_meridian_transit
from leblanc_aa_catalogues import validate


BUILD_DIR = Path("./build")
SCHEMA_URI = "https://voparis-ns.obspm.fr/maser/tfcat/v1.0/tfcat-v1.0-rc2.json"

INPUT_CATALOGUES = [
    BUILD_DIR / 'leblanc_1981_table1.json',
    BUILD_DIR / 'leblanc_1983_table1.json',
    BUILD_DIR / 'leblanc_1989_table.json',
    BUILD_DIR / 'leblanc_1990_table.json',
    BUILD_DIR / 'leblanc_1993_table1.json',
]

OUTPUT_DATA_DIRECTORY = Path('./compiled')
FINAL_CATALOGUE_VERSION = '1.0'
FINAL_CATALOGUE_DOI = '10.25935/1fqm-fs07'
MASER_CONTENT_URL = f'https://maser.obspm.fr/doi/{FINAL_CATALOGUE_DOI}/content/'


def file_name_template_obs(y, v):
    return f"leblanc_aa_catalogue_obs_{y}_v{v}.json"


def file_name_template_emi(y, v):
    return f"leblanc_aa_catalogue_emi_{y}_v{v}.json"


# Preparing TFCat metadata
fields = {
    'obs_id': {
        'info': "Observation ID in the catalogue",
        'datatype': 'int',
        'ucd': 'meta.id'
    },
    'meridian_transit': {
        'info': 'Jupiter local meridian transit time',
        'datatype': 'str',
        'ucd': 'time.epoch;obs.transit'
    },
    'cml_beg': {
        'info': 'Central Meridian Longitude at begin of interval (System III Jovian Longitude)',
        'unit': 'deg',
        'datatype': 'float',
        'ucd': 'pos.bodyrc.lon;time.start'
    },
    'cml_end': {
        'info': 'Central Meridian Longitude at end of interval (System III Jovian Longitude)',
        'unit': 'deg',
        'datatype': 'float',
        'ucd': 'pos.bodyrc.lon;time.end'
    },
    'io_phase_beg': {
        'info': 'Io Phase at begin of interval',
        'unit': 'deg',
        'datatype': 'float',
        'ucd': 'pos.angDistance;time.start'
    },
    'io_phase_end': {
        'info': 'Io Phase at end of interval',
        'unit': 'deg',
        'datatype': 'float',
        'ucd': 'pos.angDistance;time.end'
    },
    'eu_phase_beg': {
        'info': 'Europa Phase at begin of interval',
        'unit': 'deg',
        'datatype': 'float',
        'ucd': 'pos.angDistance;time.start'
    },
    'eu_phase_end': {
        'info': 'Europa Phase at end of interval',
        'unit': 'deg',
        'datatype': 'float',
        'ucd': 'pos.angDistance;time.end'
    },
    'ga_phase_beg': {
        'info': 'Ganymede Phase at begin of interval',
        'unit': 'deg',
        'datatype': 'float',
        'ucd': 'pos.angDistance;time.start'
    },
    'ga_phase_end': {
        'info': 'Ganymede Phase at end of interval',
        'unit': 'deg',
        'datatype': 'float',
        'ucd': 'pos.angDistance;time.end'
    },
    'ca_phase_beg': {
        'info': 'Callisto Phase at begin of interval',
        'unit': 'deg',
        'datatype': 'float',
        'ucd': 'pos.angDistance;time.start'
    },
    'ca_phase_end': {
        'info': 'Callisto Phase at end of interval',
        'unit': 'deg',
        'datatype': 'float',
        'ucd': 'pos.angDistance;time.end'
    },
    'am_phase_beg': {
        'info': 'Amalthea Phase at begin of interval',
        'unit': 'deg',
        'datatype': 'float',
        'ucd': 'pos.angDistance;time.start'
    },
    'am_phase_end': {
        'info': 'Amalthea Phase at end of interval',
        'unit': 'deg',
        'datatype': 'float',
        'ucd': 'pos.angDistance;time.end'
    }
}

field_types = {
    'obs_id': int,
    'cml_beg': float,
    'cml_end': float,
    'meridian_transit': float,
    'io_phase_beg': float,
    'io_phase_end': float,
    'eu_phase_beg': float,
    'eu_phase_end': float,
    'ga_phase_beg': float,
    'ga_phase_end': float,
    'ca_phase_beg': float,
    'ca_phase_end': float,
    'am_phase_beg': float,
    'am_phase_end': float
}

prev_month = -1
obs_id_offset = 0
emi_id_offset = 0

yearly_cat_obs = dict([(year, []) for year in range(1978, 1991)])
yearly_cat_emi = dict([(year, []) for year in range(1978, 1991)])
yearly_cat_file = dict([(year, []) for year in range(1978, 1991)])


def get_year(atime) -> int:
    return atime.to_datetime().year


for lebcat_file in INPUT_CATALOGUES:

    print(f"Processing {str(lebcat_file)}")

    lebcat = TFCat.from_file(lebcat_file)
    tconv = lebcat.crs.time_converter

    # Get tmin/tmax for each observation and feature:
    # each observation (lebcat.iter_*), get tmin and tmax attributes, and converter to Time objects
    tmin_obs, tmax_obs = map(tconv, zip(*[(item.tmin, item.tmax) for item in lebcat.iter_observations]))
    tmin_emi, tmax_emi = map(tconv, zip(*[(item.tmin, item.tmax) for item in lebcat.iter_features]))

    # Compute Meridian Transit time
    raw_meridian_transits = get_jupiter_meridian_transit(
        tconv(lebcat.observation(0).tmin),
        tconv(lebcat.observation(-1).tmax)
    )

    meridian_transits = []
    for t1, t2 in zip(tmin_obs, tmax_obs):
        i = numpy.where((t1 < raw_meridian_transits) & (raw_meridian_transits < t2))[0]
        cur_transit = raw_meridian_transits[i[0]] if len(i) > 0 else ''
        meridian_transits.append(cur_transit)

    # Compute CML:
    cml_obs_beg, cml_obs_end = map(get_cml, (tmin_obs, tmax_obs))
    cml_emi_beg, cml_emi_end = map(get_cml, (tmin_emi, tmax_emi))

    # Compute Io Phase:
    io_phase_obs_beg, io_phase_obs_end = map(get_jovian_moon_phase, (tmin_obs, tmax_obs), ('IO', 'IO'))
    io_phase_emi_beg, io_phase_emi_end = map(get_jovian_moon_phase, (tmin_emi, tmax_emi), ('IO', 'IO'))

    # Compute Europa Phase:
    eu_phase_obs_beg, eu_phase_obs_end = map(get_jovian_moon_phase, (tmin_obs, tmax_obs), ('EUROPA', 'EUROPA'))
    eu_phase_emi_beg, eu_phase_emi_end = map(get_jovian_moon_phase, (tmin_emi, tmax_emi), ('EUROPA', 'EUROPA'))

    # Compute Ganymede Phase:
    ga_phase_obs_beg, ga_phase_obs_end = map(get_jovian_moon_phase, (tmin_obs, tmax_obs), ('GANYMEDE', 'GANYMEDE'))
    ga_phase_emi_beg, ga_phase_emi_end = map(get_jovian_moon_phase, (tmin_emi, tmax_emi), ('GANYMEDE', 'GANYMEDE'))

    # Compute Callisto Phase:
    ca_phase_obs_beg, ca_phase_obs_end = map(get_jovian_moon_phase, (tmin_obs, tmax_obs), ('CALLISTO', 'CALLISTO'))
    ca_phase_emi_beg, ca_phase_emi_end = map(get_jovian_moon_phase, (tmin_emi, tmax_emi), ('CALLISTO', 'CALLISTO'))

    # Compute Amalthea Phase:
    am_phase_obs_beg, am_phase_obs_end = map(get_jovian_moon_phase, (tmin_obs, tmax_obs), ('AMALTHEA', 'AMALTHEA'))
    am_phase_emi_beg, am_phase_emi_end = map(get_jovian_moon_phase, (tmin_emi, tmax_emi), ('AMALTHEA', 'AMALTHEA'))

    for i, obs in enumerate(lebcat.iter_observations):
        cur_year = get_year(tconv(obs.tmin))
        yearly_cat_file[cur_year] = lebcat_file
        yearly_cat_obs[cur_year].append(
            Feature(
                id=obs.properties['obs_id'] + obs_id_offset,
                geometry=obs.geometry,
                properties={
                    'obs_id': obs.properties['obs_id'] + obs_id_offset,
                    'cml_beg': cml_obs_beg[i] % 360,
                    'cml_end': cml_obs_end[i] % 360,
                    'meridian_transit': meridian_transits[i],
                    'io_phase_beg': io_phase_obs_beg[i] % 360,
                    'io_phase_end': io_phase_obs_end[i] % 360,
                    'eu_phase_beg': eu_phase_obs_beg[i] % 360,
                    'eu_phase_end': eu_phase_obs_end[i] % 360,
                    'ga_phase_beg': ga_phase_obs_beg[i] % 360,
                    'ga_phase_end': ga_phase_obs_end[i] % 360,
                    'ca_phase_beg': ca_phase_obs_beg[i] % 360,
                    'ca_phase_end': ca_phase_obs_end[i] % 360,
                    'am_phase_beg': am_phase_obs_beg[i] % 360,
                    'am_phase_end': am_phase_obs_end[i] % 360,
                }
            )
        )

    for i, emi in enumerate(lebcat.iter_features):
        cur_year = get_year(tconv(emi.tmin))
        yearly_cat_file[cur_year] = lebcat_file
        yearly_cat_emi[cur_year].append(
            Feature(
                id=i+emi_id_offset,
                geometry=emi.geometry,
                properties={
                    'obs_id': emi.properties['obs_id'] + obs_id_offset,
                    'cml_beg': cml_emi_beg[i],
                    'cml_end': cml_emi_end[i],
                    'meridian_transit': meridian_transits[emi.properties['obs_id']],
                    'io_phase_beg': io_phase_emi_beg[i],
                    'io_phase_end': io_phase_emi_end[i],
                    'eu_phase_beg': eu_phase_emi_beg[i],
                    'eu_phase_end': eu_phase_emi_end[i],
                    'ga_phase_beg': ga_phase_emi_beg[i],
                    'ga_phase_end': ga_phase_emi_end[i],
                    'ca_phase_beg': ca_phase_emi_beg[i],
                    'ca_phase_end': ca_phase_emi_end[i],
                    'am_phase_beg': am_phase_emi_beg[i],
                    'am_phase_end': am_phase_emi_end[i],
                }
            )
        )
    obs_id_offset += lebcat.observation(-1).properties['obs_id']
    emi_id_offset += lebcat.feature(-1).properties['emi_id']


output_catalogue_config = {
    'emi': (yearly_cat_emi, file_name_template_emi, 'Events'),
    'obs': (yearly_cat_obs, file_name_template_obs, 'Observations')
}


def write_catalogues(cat_type: str, yearly_cat_file):

    yearly_catalogue, file_name_template, table_header_string = output_catalogue_config[cat_type]

    print(f'|{{{{Year}}}}|{{{{# of {table_header_string}}}}}|{{{{Size (MB)}}}}|{{{{File}}}}|{{{{Start Time}}}}|{{{{End Time}}}}|')
    for year, year_catalogue in yearly_catalogue.items():

        if len(year_catalogue) != 0:
            year_file = yearly_cat_file[year]
            lebcat = TFCat.from_file(year_file)
            year_collection = FeatureCollection(
                schema=SCHEMA_URI,
                features=year_catalogue,
                properties=lebcat.properties or None,
                fields=fields,
                crs=lebcat.crs
            )
            year_collection.properties['parent'], year_collection.properties['doi'] = \
            year_collection.properties['doi'], f"https://doi.org/{FINAL_CATALOGUE_DOI}"

            year_collection.properties['time_min'] = lebcat.crs.time_converter(year_catalogue[0].tmin).isot
            year_collection.properties['time_max'] = lebcat.crs.time_converter(year_catalogue[-1].tmax).isot

            output_file = OUTPUT_DATA_DIRECTORY / file_name_template(year, FINAL_CATALOGUE_VERSION)
            output_file.parent.mkdir(exist_ok=True)

            with open(output_file, 'w') as f:
                dump(year_collection, f)
                file_size = os.path.getsize(output_file) / (1024 * 1024)
                print(f'|{year}|{len(year_catalogue)}|{file_size:.2f}|'+
                      f'[{Path(output_file).name}->{MASER_CONTENT_URL}{Path(output_file).name}]|' +
                      f'{year_collection.properties["time_min"]}|{year_collection.properties["time_max"]}|')

            validate(output_file)

        else:
            print(f'|{year}|{len(year_catalogue)}||||')


write_catalogues('emi', yearly_cat_file)
write_catalogues('obs', yearly_cat_file)

print(f"scp {OUTPUT_DATA_DIRECTORY}/*.json vmmaser_rpws:/volumes/kronos/doi/{FINAL_CATALOGUE_DOI}/content/")