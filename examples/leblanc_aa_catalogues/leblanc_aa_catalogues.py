#!/usr/bin/env python
"""
This script produces TFCat JSON files from the Leblanc et al., A&AS catalogues (1981 to 1993)
"""

__author__ = 'Baptiste Cecconi'
__copyright__ = 'Copyright 2021, Baptiste Cecconi, PADC/MASER'
__credits__ = ['Baptiste Cecconi']
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = 'Baptiste Cecconi'
__email__ = 'baptiste.cecconi@obspm.fr'
__status__ = "development"


import numpy
import datetime
from tfcat import TFCat, FeatureCollection, Feature, Polygon
from astropy.time import Time
from astropy.units import Unit
from collections import namedtuple
import urllib.request
import json
import pprint
from pathlib import Path

BUILD_DIR = Path("./build")
SCHEMA_URI = "https://voparis-ns.obspm.fr/maser/tfcat/v1.0/tfcat-v1.0-rc2.json"

Leblanc_AA_Catalogues = {
    "leblanc_1981": {
        "url": "https://maser.obspm.fr/doi/10.25935/gxzf-zt33/content/leblanc_1981_table1.txt",
        "doi": "https://doi.org/10.25935/GXZF-ZT33"
    },
    "leblanc_1983": {
        "url": "https://maser.obspm.fr/doi/10.25935/ras0-er93/content/leblanc_1983_table1.txt",
        "doi": "https://doi.org/10.25935/RAS0-ER93"
    },
    "leblanc_1989": {
        "url": "https://maser.obspm.fr/doi/10.25935/403b-va51/content/leblanc_1989_table.txt",
        "doi": "https://doi.org/10.25935/403B-VA51"
    },
    "leblanc_1990": {
        "url": "https://maser.obspm.fr/doi/10.25935/gh59-py87/content/leblanc_1990_table.txt",
        "doi": "https://doi.org/10.25935/gh59-py87"
    },
    "leblanc_1993": {
        "url": "https://maser.obspm.fr/doi/10.25935/cmqb-jj10/content/leblanc_1993_table1.txt",
        "doi": "https://doi.org/10.25935/cmqb-jj10"
    }
}


def validate(file_name):
    from tfcat.validate import validate_file
    validate_file(file_name, schema_uri="https://voparis-ns.obspm.fr/maser/tfcat/v1.0/tfcat-v1.0-rc2.json")


def load_catalogue(file_name, url=False, verbose=False):

    # Define Columns (names and data types) in the order of the catalogue table
    dtype_catalogue_obs = [
        ('OBS_ID', int),
        ('DATE_BEG', datetime.datetime),
        ('DATE_END', datetime.datetime),
        ('YYMMDD', numpy.string_, 8),
        ('DOY', int),
        ('HHMM_BEG', int),
        ('HHMM_END', int),
        ('CML_BEG', int),
        ('CML_END', int),
        ('IO_PHASE_BEG', int),
        ('IO_PHASE_END', int),
        ('FREQ_MIN', int),
        ('FREQ_MAX', int)]

    dtype_catalogue_emi = [
        ('EMI_ID', int),
        ('OBS_ID', int),
        ('DATE_BEG', datetime.datetime),
        ('DATE_END', datetime.datetime),
        ('HHMM_BEG', int),
        ('HHMM_END', int),
        ('CML_BEG', int),
        ('CML_END', int),
        ('IO_PHASE_BEG', int),
        ('IO_PHASE_END', int),
        ('FREQ_MIN', int),
        ('FREQ_MAX', int)]

    ObsItems = namedtuple("ObsItems",
                          "yymmdd doy hhmm_beg hhmm_end cml_beg cml_end iophase_beg iophase_end freq_min freq_max")
    EmiItems = namedtuple("EmiItems",
                          "hhmm_beg, hhmm_end, cml_beg, cml_end, iophase_beg, iophase_end, freq_min, freq_max")
    DateYMD = namedtuple("DateYMD", ["yy", "mm", "dd"])

    # Empty catalogue seed with columns correctly defined
    catalogue_obs = numpy.empty((0,), dtype=numpy.dtype(dtype_catalogue_obs))
    catalogue_emi = numpy.empty((0,), dtype=numpy.dtype(dtype_catalogue_emi))

    def decode_line(line: str) -> list:
        # splitting elements in pieces
        # removing multiple blank spaces on the fly, with the split/join/split trick
        # removing as well the '-' elements
        return (" ".join([item for item in line.strip().split() if item != '-'])).split()

    def decode_line_obs(cur_items: list) -> ObsItems:
        # yymmdd, doy, hhmm_beg, hhmm_end, cml_beg, cml_end, iophase_beg, iophase_end, freq_min, freq_max
        return ObsItems(cur_items[0], int(cur_items[1]), int(cur_items[2]), int(cur_items[3]), int(cur_items[4]), int(
            cur_items[5]), int(cur_items[6]), int(cur_items[7]), int(cur_items[8]), int(cur_items[9]))

    def decode_line_emi(cur_items: list) -> EmiItems:
        # hhmm_beg, hhmm_end, cml_beg, cml_end, iophase_beg, iophase_end, freq_min, freq_max
        return EmiItems(*map(int, cur_items))

    def timedelta_hhmm(hhmm: int) -> datetime.timedelta:
        return datetime.timedelta(hours=int(hhmm) // 100, minutes=int(hhmm) % 100)

    def datetime_interval(cur_date: datetime.datetime, hhmm_beg: int, hhmm_end: int) -> tuple:
        return cur_date + timedelta_hhmm(hhmm_beg), cur_date + timedelta_hhmm(hhmm_end)

    def open_file(name, url=False):

        if url:
            import ssl
            ctx = ssl.create_default_context()
            ctx.check_hostname = False
            ctx.verify_mode = ssl.CERT_NONE

            return urllib.request.urlopen(name, context=ctx)
        else:
            return open(name, 'r')

    # Reading file
    with open_file(file_name, url=url) as f:
        cur_line = 3
        cur_obs_id = -1
        cur_emi_id = -1
        for line in f.readlines()[3:]:  # simple way to skip 3 lines at the top of the file (header section)
            if isinstance(line, bytes):
                line = line.decode('ascii')

            if verbose:
                print("{:04d}: {}".format(cur_line, line))
            cur_line += 1

            # loading items from current line:
            cur_items = decode_line(line)

            if verbose:
                print(cur_items)

            # Cases:
            # - len = 10: current line has observation data but not feature
            # - len = 18: current line has observation data and feature
            # - len = 8: current line has no observation (must be previously defined) data but has feature
            # - len = 0: skip line

            if len(cur_items) in [10, 18]:

                # New observation detected, increment Obs_ID:
                cur_obs_id += 1

                # Decode observation:
                cur_catalogue_entry_tmp = decode_line_obs(cur_items)

                # Compute DateBeg and DateEnd
                cur_yymmdd = DateYMD(*map(int, cur_catalogue_entry_tmp.yymmdd.split('/')))
                cur_date_dt = datetime.datetime(year=cur_yymmdd.yy + 1900, month=cur_yymmdd.mm, day=cur_yymmdd.dd)
                cur_dt_beg, cur_dt_end = datetime_interval(
                    cur_date_dt,
                    cur_catalogue_entry_tmp.hhmm_beg,
                    cur_catalogue_entry_tmp.hhmm_end
                )

                # Prepare data ingestion
                cur_catalogue_entry = (cur_obs_id, cur_dt_beg, cur_dt_end) + cur_catalogue_entry_tmp
                if verbose:
                    print(cur_catalogue_entry)

                # Check if merging is possible (happens mostly when: previous HHMM_END=2400 and current HHMM_BEG=0000)
                if (cur_obs_id > 1) and (catalogue_obs[-1]["DATE_END"] == cur_dt_beg) and \
                        (catalogue_obs[-1]["FREQ_MIN"] == cur_catalogue_entry_tmp.freq_min) and \
                        (catalogue_obs[-1]["FREQ_MAX"] == cur_catalogue_entry_tmp.freq_max):
                    # then merge (updating only "_END" fields):
                    if verbose:
                        print("merging entries...")
                    cur_obs_id -= 1
                    catalogue_obs[-1]["DATE_END"] = cur_dt_end
                    catalogue_obs[-1]["HHMM_END"] = cur_catalogue_entry_tmp.hhmm_end
                    catalogue_obs[-1]["CML_END"] = cur_catalogue_entry_tmp.cml_end
                    catalogue_obs[-1]["IO_PHASE_END"] = cur_catalogue_entry_tmp.iophase_end
                else:
                    # else add new record
                    catalogue_obs = numpy.append(
                        catalogue_obs,
                        numpy.array(cur_catalogue_entry, dtype=catalogue_obs.dtype)
                    )

            if len(cur_items) in [8, 18]:

                # New feature detected, decode corresponding raw data chunk:
                if len(cur_items) == 18:
                    cur_catalogue_entry_tmp = decode_line_emi(cur_items[10:])
                else:
                    cur_catalogue_entry_tmp = decode_line_emi(cur_items)

                cur_emi_id += 1

                if verbose:
                    print(cur_catalogue_entry_tmp)

                # Compute DateBeg and DateEnd
                cur_dt_beg, cur_dt_end = datetime_interval(
                    cur_date_dt,
                    cur_catalogue_entry_tmp.hhmm_beg,
                    cur_catalogue_entry_tmp.hhmm_end
                )

                # Prepare data ingestion
                cur_catalogue_entry = (cur_emi_id, cur_obs_id, cur_dt_beg, cur_dt_end) + cur_catalogue_entry_tmp
                if verbose:
                    print(cur_catalogue_entry)

                # Check if merging is possible (happens mostly when: previous HHMM_END=2400 and current HHMM_BEG=0000)
                if (len(catalogue_emi) > 1) and (catalogue_emi["DATE_END"][-1] == cur_dt_beg) and \
                        (catalogue_emi[-1]["FREQ_MIN"] == cur_catalogue_entry_tmp.freq_min) and \
                        (catalogue_emi[-1]["FREQ_MAX"] == cur_catalogue_entry_tmp.freq_max):
                    # then merge (updating only "_END" fields):
                    if verbose:
                        print("merging entries...")
                    catalogue_emi[-1]["DATE_END"] = cur_dt_end
                    catalogue_emi[-1]["HHMM_END"] = cur_catalogue_entry_tmp.hhmm_end
                    catalogue_emi[-1]["CML_END"] = cur_catalogue_entry_tmp.cml_end
                    catalogue_emi[-1]["IO_PHASE_END"] = cur_catalogue_entry_tmp.iophase_end
                else:
                    # else add new record
                    catalogue_emi = numpy.append(catalogue_emi, numpy.array(cur_catalogue_entry, dtype=catalogue_emi.dtype))

    nlines_obs = len(catalogue_obs)
    nlines_emi = len(catalogue_emi)

    print("Loaded {} observations from {}".format(nlines_obs, file_name))
    print("Loaded {} emissions from {}".format(nlines_emi, file_name))
    return catalogue_obs, catalogue_emi


def bbox(date_beg, date_end, freq_min, freq_max):
    return (
        Time(date_beg),
        Time(date_end),
        freq_min * Unit('MHz'),
        freq_max * Unit('MHz')
    )


# Preparing TFCat metadata
fields_obs = {
    'obs_id': {
        'info': "Observation ID in the catalogue",
        'datatype': 'int',
        'ucd': 'meta.code.id'
    },
    'cml_beg': {
        'info': 'Central Meridian Longitude at begin of interval (System III Jovian Longitude)',
        'unit': 'deg',
        'datatype': 'float',
        'ucd': 'pos.bodyrc.lon;time.start'
    },
    'cml_end': {
        'info': 'Central Meridian Longitude at end of interval (System III Jovian Longitude)',
        'unit': 'deg',
        'datatype': 'float',
        'ucd': 'pos.bodyrc.lon;time.end'
    },
    'io_phase_beg': {
        'info': 'Io Phase at begin of interval',
        'unit': 'deg',
        'datatype': 'float',
        'ucd': 'pos.angDistance;time.start'
    },
    'io_phase_end': {
        'info': 'Io Phase at end of interval',
        'unit': 'deg',
        'datatype': 'float',
        'ucd': 'pos.angDistance;time.end'
    }
}

fields_emi = dict(**fields_obs)
fields_emi['emi_id'] = {
    'info': "Emission ID in the catalogue",
    'datatype': 'int',
    'ucd': 'meta.code.id'
}

field_types_obs = {
    'obs_id': int,
    'cml_beg': float,
    'cml_end': float,
    'io_phase_beg': float,
    'io_phase_end': float
}

field_types_emi = dict(**field_types_obs)
field_types_emi['emi_id'] = int


def to_feature_obs(row):
    return _to_feature(row, int(row['OBS_ID']), fields_obs, field_types_obs)


def to_feature_emi(row):
    return _to_feature(row, int(row['EMI_ID']), fields_emi, field_types_emi)


def _to_feature(row, id, fields, field_types):
    """
    Convert a Row of the Obs table into a Feature

    :param row: input Obs row from
    :type numpy.ndarray
    :return: a feature with the Obs geometry
    :rtype: Feature
    """

    # Extracting bbox (e.g., tmin/tmax/fmin/fmax)
    tmin, tmax, fmin, fmax = \
        bbox(row["DATE_BEG"], row["DATE_END"], row['FREQ_MIN'], row['FREQ_MAX'])

    return Feature(
        id=id,
        geometry=Polygon.from_bbox([tmin.unix, tmax.unix], [fmin.value, fmax.value]),
        properties=dict(
            (item, field_types[item](row[item.upper()])) for item in fields.keys()
        )
    )


crs = {
    "type": "local",
    "properties": {
        "name": "Time-Frequency",
        "time_coords_id": "unix",
        "spectral_coords": {
            "type": "frequency",
            "unit": "MHz"
        },
        "ref_position_id": "GEOCENTER"
    }
}


def maser_schema_org(doi):
    striped_doi = doi.replace('https://doi.org/', '').lower()
    schema_org_url = f'https://maser.obspm.fr/doi/{striped_doi}/schema_org.json'
    print(f'Fetching metadata from: {schema_org_url}')
    with urllib.request.urlopen(schema_org_url) as f:
        raw_text = f.read().decode('utf-8')
        schema_org = json.loads(raw_text)
    return schema_org


if __name__ == "__main__":
    for catalogue_name, catalogue_meta in Leblanc_AA_Catalogues.items():
        print(f'Processing: {catalogue_name}')
        print(f'URL: {catalogue_meta["url"]}')
        print(f'DOI: {catalogue_meta["doi"]}')

        schema_org = maser_schema_org(catalogue_meta["doi"])
        raw_obs, raw_emi = load_catalogue(catalogue_meta["url"], url=True, verbose=False)

        properties = {
            "instrument_host_name": "Nançay Decameter Array",
            "instrument_name": "Routine",
            "title": schema_org['name'],
            "authors": schema_org['author'] + schema_org['editor'],
            "doi": catalogue_meta["doi"],
            "bib_reference": schema_org["@reverse"]["isBasedOn"]['identifier']['value'],
            "publisher": "PADC/MASER",
            "version": "1.0",
            "description": schema_org["description"],
            "target_name": "Jupiter",
            "target_class": "planet",
            "target_region": "magnetosphere",
            "feature_name": "radio emissions"
        }

        # print properties (metadata)
        pprint.pprint(properties)

        catalogue_collection = FeatureCollection(
            schema=SCHEMA_URI,
            features=list(map(to_feature_emi, raw_emi)),
            observations=list(map(to_feature_obs, raw_obs)),
            properties=properties,
            fields=fields_emi,
            crs=crs
        )

        # take URL, extract .txt file name, and rename .json
        tfcat_json_file = catalogue_meta['url'].split('/')[-1].replace('.txt', '.json')

        print(f'Writing file: {BUILD_DIR / tfcat_json_file}')
        with open(BUILD_DIR / tfcat_json_file, 'w') as f:
            from tfcat import dump
            dump(catalogue_collection, f)

        print(f'Validate file: {BUILD_DIR / tfcat_json_file}')
        validate(BUILD_DIR / tfcat_json_file)
