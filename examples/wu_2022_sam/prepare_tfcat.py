#!/usr/bin/env python3
# -*- encode: utf-8 -*-

import openpyxl
from pathlib import Path
import numpy
from astropy.time import Time
from astropy.units import Unit
import functools
from tfcat import Feature, Polygon, CRS, FeatureCollection, dump
from shapely.geometry import shape
from tfcat.validate import validate_file

INPUT_FILE = Path('SAM_Supplementary1_list_v1031.xlsx')
SCHEMA_URI = "https://voparis-ns.obspm.fr/maser/tfcat/v1.0/tfcat-v1.0-rc2.json"
DOI = "10.25935/8may-4320"


def reader(xlsx_file: Path or str) ->  numpy.array:
    """
    Reads raw data from XLSX file
    :param xlsx_file: input file
    :return:
    """

    # open up the XLSX file, in read only mode (to speed up the process)
    wb = openpyxl.load_workbook(xlsx_file, read_only=True)
    ws = wb['Sheet1']

    # loads all rows (and stop at the first empty row)
    rows = ws.rows
    raw_data = []
    header = [cell.value for cell in next(rows)]
    for row in rows:
        cells = [cell.value for cell in row]
        if cells[0] is None:
            break
        else:
            raw_data.append(cells)
    wb.close()

    # convert into usable quantities
    data = []
    for line in raw_data:
        data.append([
            line[0],
            Time(line[1].replace('T', ' ').strip()),
            Time(line[2].replace('T', ' ').strip()),
            line[3] * Unit('kHz'),
            line[4] * Unit('kHz'),
            functools.reduce(lambda a, b: float(a)*float(b)/10,
                             line[5].strip().split('×')) * Unit('V**2 m**-2 Hz**-1'),
            line[6] == 1,
            line[7] == 1,
            line[8] == 1 if isinstance(line[8], int) else None
        ])

    return numpy.array(data)


def converter(data: numpy.ndarray):
    """
    Converts the inout data into a TFCat Feature Collection

    :param data:
    :return:
    """

    # loop on data to fill the event list for Feature objects
    events = []
    for seq, tmin, tmax, fmin, fmax, spec, harm2, lfe, ena in data:
        events.append(Feature(
            id=seq,
            geometry=Polygon.from_bbox([tmin.unix, tmax.unix], [fmin.value, fmax.value]),
            properties={
                'spectral_density': spec.value,
                '2nd_harmonic': harm2,
                'lfe': lfe,
                'ena': ena
            },
        ))

    # define the global properties
    properties = {
        "instrument_host_name": "Cassini",
        "instrument_name": "RPWS",
        "receiver_name": "HFR",
        "title": "Catalogue of Saturn Anomalous Myriametric Radiation, "
                 "A New Type of Saturn Radio Emission Revealed by Cassini",
        "authors": [{
            "GivenName": "Si Yuan",
            "FamilyName": "Wu",
            "ORCID": "https://orcid.org/0000-0002-1326-8829",
            "Affiliation": "https://ror.org/049tv2d57"
        }, {
            "GivenName": "Sheng Yi",
            "FamilyName": "Ye",
            "ORCID": "https://orcid.org/0000-0002-3064-1082",
            "Affiliation": "https://ror.org/02eptjh02"
        }, {
            "GivenName": "Georg",
            "FamilyName": "Fischer",
            "ORCID": "https://orcid.org/0000-0002-0431-2381",
            "Affiliation": "https://ror.org/002w0jp71"
        }, {
            "GivenName": "Ulrich",
            "FamilyName": "Taubenschuss",
            "ORCID": "https://orcid.org/0000-0003-4810-6958",
            "Affiliation": "https://ror.org/04vtzcr32"
        }, {
            "GivenName": "Caitriona",
            "FamilyName": "Jackman",
            "ORCID": "https://orcid.org/0000-0003-4810-6958",
            "Affiliation": "https://ror.org/051sx6d27"
        }, {
            "GivenName": "Elizabeth",
            "FamilyName": "O'Dwyer",
            "Affiliation": "https://ror.org/051sx6d27"
        }, {
            "GivenName": "William, S.",
            "FamilyName": "Kurth",
            "ORCID": "https://orcid.org/0000-0002-5471-6202",
            "Affiliation": "https://ror.org/036jqmy94"
        }, {
            "GivenName": "Z. H.",
            "FamilyName": "Zao",
            "Affiliation": "https://ror.org/04gcegc37"
        }, {
            "GivenName": "S.",
            "FamilyName": "Yao",
            "Affiliation": "https://ror.org/034t30j35"
        }, {
            "GivenName": "J. Douglas",
            "FamilyName": "Menietti",
            "ORCID": "https://orcid.org/0000-0001-6737-251X",
            "Affiliation": "https://ror.org/036jqmy94"
        }, {
            "GivenName": "Y.",
            "FamilyName": "Xu",
            "Affiliation": "https://ror.org/033vjfk17"
        }, {
            "GivenName": "Baptiste",
            "FamilyName": "Cecconi",
            "ORCID": "https://orcid.org/0000-0001-7915-5571",
            "Affiliation": "https://ror.org/029nkcm90"
        }],
        "bib_reference": "https://doi.org/10.1029/2022GL099237",
        "doi": f"https://doi.org/{DOI}",
        "publisher": "MASER/PADC",
        "data_source_reference": "https://doi.org/10.25935/5jfx-dh49",
        "version": "1.0",
        "target_name": "Saturn",
        "target_class": "planet",
        "target_region": "magnetosphere",
        "feature_name": "Saturn Anomalous Myriametric Radiation#SAM#Radio Emissions"
        # TODO: add spectral_range_min, spectral_range_max, time_min, time_max (using shapely?)
    }

    # define fields
    fields = {
        "spectral_density": {
            "info": "Measured Spectral Density",
            "datatype": "float",
            "unit": "V**2.m**-2.Hz**-1",
            "ucd": "phot.flux.density;em.radio"
        },
        "2nd_harmonic": {
            "info": "Presence of second harmonic emission",
            "datatype": "bool",
            "ucd": "meta.code.class"
        },
        "lfe": {
            "info": "Presence of Low Frequency Extension (LFE)",
            "datatype": "bool",
            "ucd": "meta.code.class"
        },
        "ena": {
            "info": "Presence of Energetic Neutral Atoms (ENA) data",
            "datatype": "bool",
            "ucd": "meta.code.class"
        },
    }

    # define Coordinate Reference System
    crs = CRS({
        "type": "local",
        "properties": {
            "name": "Time-Frequency",
            "time_coords_id": "unix",
            "spectral_coords": {
                "type": "frequency",
                "unit": "kHz"
            },
            "ref_position_id": "cassini-orbiter"
        }
    })

    # Fill in a FeatureCollection object
    collection = FeatureCollection(
        schema=SCHEMA_URI,
        features=events,
        properties=properties,
        fields=fields,
        crs=crs
    )

    return collection


def writer(collection, file_name=f"{INPUT_FILE.stem}.json"):
    # writes out a TFCat JSON file
    with open(file_name, 'w') as f:
        dump(collection, f)

    validate_file(file_name, SCHEMA_URI)
    print(f'scp {file_name} vmmaser_rpws:/volumes/kronos/doi/{DOI}/content/data')


if __name__ == "__main__":
    data = reader(INPUT_FILE)
    collection = converter(data)
    writer(collection)
    print("")
