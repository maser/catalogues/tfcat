#!/usr/bin/env python
"""
This script produces TFCat JSON files from the HELIO HFC T3 Catalogue.

We load the HFC T3 data using a REST PQL query.
"""

__author__ = 'Baptiste Cecconi'
__copyright__ = 'Copyright 2021, Baptiste Cecconi, PADC/MASER'
__credits__ = ['Baptiste Cecconi']
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = 'Baptiste Cecconi'
__email__ = 'baptiste.cecconi@obspm.fr'
__status__ = "development"


import urllib.request
import urllib.parse
import datetime
import astropy.io.votable.tree
from astropy.io.votable import parse
from io import BytesIO
from tfcat import FeatureCollection, Feature, Polygon, LineString, CRS, dump
from astropy.time import Time
from astropy.units import Unit
from sunpy.roi.chaincode import Chaincode
import numpy

HFC_OBSERVATORIES = {
    "WIND": 21,
    "STEREO_A": 24,
    "STEREO_B": 25
}

HELIO_HFC_PQL_URL = "http://voparis-helio.obspm.fr/helio-hfc/HelioQueryService"

HELIO_HFC_T3_CRS = CRS({
    "type": "local",
    "properties": {
        "name": "Time-Frequency",
        "time_coords_id": "unix",
        "spectral_coords": {
            "type": "frequency",
            "unit": "MHz"
        },
        "ref_position_id": "wind"
    }
})

SCHEMA_URI = "https://voparis-ns.obspm.fr/maser/tfcat/v1.0/tfcat-v1.0-rc2.json"


def helio_hfc_t3_hqi_pql_query_url(
        observatory: str,
        start_time: datetime.datetime,
        end_time: datetime.datetime) -> str:
    """
    Construct the PQL query URL for an observatory and a time interval. All columns from VIEW_T3_HQI are loaded.

    :param observatory: Spacecraft name (allowed values: 'WIND', 'STEREO_A', 'STEREO_B')
    :param start_time: start time of query
    :param end_time: end time of query
    :return: the REST URL to be queried
    """

    if observatory not in HFC_OBSERVATORIES.keys():
        raise ValueError('Unknown observatory name')

    try:
        str_start_time = start_time.strftime("%Y-%m-%dT%H:%M:%S")
        str_end_time = end_time.strftime("%Y-%m-%dT%H:%M:%S")
    except AttributeError:
        raise ValueError('Start_time and end_time should be datetime objects')

    pql_query = f"FROM=VIEW_T3_HQI&STARTTIME={str_start_time}&ENDTIME={str_end_time}&WHERE=OBSERVAT,{observatory}"

    return HELIO_HFC_PQL_URL+'?'+pql_query


def get_table(query: str) -> astropy.io.votable.tree.Table:
    """
    Runs a PQL query on HELIO HFC server and return the data table

    Since the astropy.io.votable parser takes a file (or file-like) as input, we have to read from URL and
    write into in intermediate BytesIO buffer.

    :param query: PQL query full URL string
    :return: a table containing the query results
    """

    with urllib.request.urlopen(query) as u:
        data = BytesIO()
        for line in u.readlines():
            data.write(line)
    return parse(data).get_first_table()


def cc_x_pix_to_time(cc_x_pix, nx, cc_x_start):
    """
    Converts a pixel coordinate into a time stamp

    :param cc_x_pix: chain code coordinate on time axis
    :type: float
    :param nx: number of pixels in time axis
    :type: int
    :param cc_x_start: starting point of time axis (date_obs)
    :type: str
    :return: real time coordinate in unix Time, as defined in CRS
    :rtype: float
    """
    return (Time(cc_x_start) + cc_x_pix/nx*Unit('d')).unix


def cc_y_pix_to_freq(cc_y_pix, ny, cc_y_min, cc_y_max):
    """
    Converts a pixel coordinate into a frequency

    :param cc_y_pix: chain code coordinate on spectral axis
    :type: float
    :param nx: number of pixels in time axis
    :type: int
    :param cc_x_min: lower bound of spectral axis (MHz)
    :type: float
    :param cc_x_max: upper bound of spectral axis (MHz)
    :type: float
    :return: real spectral coordinate, as defined in CRS
    :rtype: float
    """
    return ((cc_y_pix/ny*(cc_y_max-cc_y_min) + cc_y_min) * Unit('MHz')).to(
        HELIO_HFC_T3_CRS.properties['spectral_coords']['unit']
    ).value


def cc_geometry(cc: str, cc_t0: int, cc_f0: int, nt: int, nf: int, tmin: str, fmin: float, fmax: float, geometry_class):
    """
    Decoding the chaincode and writing out a TFCat geometry

    :param cc: chaincode
    :param cc_t0: time starting point (pixel coordinate)
    :param cc_f0: spectral starting point (pixel coordinate)
    :param nt: time axis size (pixels)
    :param nf: spectral axis size (pixels)
    :param tmin: time starting point (ISO8601 format)
    :param fmin: spectral axis min value (MHz)
    :param fmax: spectral axis max value (MHz)
    :param geometry_class: TFCat geometry class
    :return: TFCat geometry object
    """

    cc = Chaincode([cc_t0, cc_f0], cc)
    coords = list(zip(
        map(lambda x: cc_x_pix_to_time(x, nt, tmin), cc.coordinates[0]),
        map(lambda x: cc_y_pix_to_freq(x, nf, fmin, fmax), cc.coordinates[1])
    ))
    return geometry_class([coords]) if geometry_class == Polygon else geometry_class(coords)


def _event(row: dict, cc_var: str, geometry_type) -> Feature:
    """
    converts a row into a Feature

    :param row:
    :param cc_var: Chaincode prefix name ('cc' or 'ske_cc')
    :param geometry_type:
    :return:
    """
    def fix_json_value(value):
        if isinstance(value, numpy.int32):
            return int(value)
        elif value is numpy.ma.masked:
            return None
        elif isinstance(value, numpy.float32):
            return float(value)
        else:
            return value

    properties = dict(
        [(key, fix_json_value(value)) for key, value in row.items()]
    )

    geometry = cc_geometry(
        row[cc_var],
        row[cc_var+'_x_pix'],
        row[cc_var+'_y_pix'],
        row['naxis1'],
        row['naxis2'],
        row['date_obs'],
        row['freqmin'],
        row['freqmax'],
        geometry_type
    )

    return Feature(
        id=int(row['id_type_iii']),
        geometry=geometry,
        properties=properties
    )


def event_contour(row: dict) -> Feature:
    """
    Produce an event contour Feature

    :param row: VIEW_T3_HQI row
    :return: a TFCat Feature with a Polygon geometry
    """
    return _event(row, 'cc', Polygon)


def event_skeleton(row: dict) -> Feature:
    """
    Produce an event skeleton Feature

    :param row: VIEW_T3_HQI row
    :return: a TFCat Feature with a LineString geometry
    """
    return _event(row, 'ske_cc', LineString)


def fields(table_fields):
    tmp_fields = {}
    for cur_field in table_fields:
        tmp_fields[cur_field.name] = {
            'datatype': cur_field.datatype,
            'ucd': cur_field.ucd
        }


def validate(file_name):
    from tfcat.validate import validate_file
    validate_file(file_name, schema_uri=SCHEMA_URI)


if __name__ == "__main__":
    # Prepare test query
    obs_date = datetime.datetime(1996, 1, 11)
    observatory = 'WIND'

    print(f'Fetching {observatory} data for date: {obs_date.isoformat()}')
    table = get_table(
        helio_hfc_t3_hqi_pql_query_url(
            observatory,
            obs_date,
            obs_date + datetime.timedelta(days=1)
        )
    )

    contours = []
    skeletons = []
    for row in table.array:
        contours.append(event_contour(dict(zip(table.array.dtype.names, row))))
        skeletons.append(event_skeleton(dict(zip(table.array.dtype.names, row))))

    contour_collection = FeatureCollection(
        schema=SCHEMA_URI,
        features=contours,
        fields=fields(table.fields),
        crs=HELIO_HFC_T3_CRS
    )

    file_name = f'helio_hfc_t3_contours_{obs_date.date().isoformat()}.json'
    print(f'Writing file: {file_name}')
    with open(file_name, 'w') as f:
        dump(contour_collection, f)
    print(f'Validating file: {file_name}')
    validate(file_name)

    skeleton_collection = FeatureCollection(
        schema=SCHEMA_URI,
        features=skeletons,
        fields=fields(table.fields),
        crs=HELIO_HFC_T3_CRS
    )

    file_name = f'helio_hfc_t3_skeletons_{obs_date.date().isoformat()}.json'
    print(f'Writing file: {file_name}')
    with open(file_name, 'w') as f:
        dump(skeleton_collection, f)
    print(f'Validating file: {file_name}')
    validate(file_name)
