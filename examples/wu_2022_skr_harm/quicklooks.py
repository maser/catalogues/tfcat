#!/usr/bin/env python3
# -*- encode: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.colors as colors
from pathlib import Path
from tfcat import TFCat, Feature, CRS
from shapely.geometry import MultiPoint, Point, Polygon
import das2
from astropy.time import Time
import numpy

DAS2_SRC_KRONOS_N3E_V = das2.get_source("tag:das2.org,2012:site:/voparis/cassini-rpws-hfr/n3e/kronos_n3e_v/das2")
DAS2_SRC_KRONOS_N3E_S = das2.get_source("tag:das2.org,2012:site:/voparis/cassini-rpws-hfr/n3e/kronos_n3e_s/das2")
TFCAT_FILE = Path(__file__).parent / "wu_2022_table_a1.json"


def mask_polygon(times: numpy.ndarray, freqs: numpy.ndarray, data: numpy.ndarray, polygon: Polygon):
    """produce a masked dataset from an input polygon

    :param times: time coordinate for each data pixel
    :param freqs: frequency coordinate for each data pixel
    :param data: data array
    :param polygon: data selection polygon
    :return:
    """

    coords = [(t, f) for t, f in zip(times.flatten(), freqs.flatten())]
    shape = data.shape
    data = data.flatten()
    data_len = len(data)
    index = numpy.arange(data_len, dtype=int)
    data_points = MultiPoint([Point(x, y, z) for (x, y), z in zip(coords, index)])
    select_points = polygon.intersection(data_points)
    mask = numpy.zeros((data_len,))
    mask[[int(geom.z) for geom in select_points.geoms]] = 1
    mask = (mask == 0)
    return numpy.ma.masked_array(data, mask=mask)


def plot_event(feature: Feature, crs: CRS):
    tmin = crs.time_converter(feature.bbox[0]).isot
    tmax = crs.time_converter(feature.bbox[2]).isot
    n3e_v = DAS2_SRC_KRONOS_N3E_V.get({'time': (tmin, tmax, 4.0)})[0]
    n3e_s = DAS2_SRC_KRONOS_N3E_S.get({'time': (tmin, tmax, 4.0)})[0]

    fund_polygon = Polygon(feature.geometry.coordinates[0][0])
    harm_polygon = Polygon(feature.geometry.coordinates[1][0])

    times = n3e_v['time']['center'][...].value
    freqs = n3e_v['frequency']['center'][...].value
    data_v = n3e_v['v']['center'][...].value
    data_s = n3e_s['s']['center'][...].value

    fig, (ax1, ax2) = plt.subplots(2,1)
    scaleZ = colors.Normalize(vmin=-1.5, vmax=1.5)
    ax1.set_yscale('log')
    ax1.pcolormesh(times, freqs, data_v, norm=scaleZ, cmap='jet')
    ax1.plot(crs.time_converter(fund_polygon.exterior.xy[0]).datetime, fund_polygon.exterior.xy[1], 'k')
    ax1.plot(crs.time_converter(harm_polygon.exterior.xy[0]).datetime, harm_polygon.exterior.xy[1], '--b')
    ax1.set_ylim((100, 1500))

    scaleZ = colors.Normalize(vmin=numpy.min(numpy.log(data_s)), vmax=numpy.max(numpy.log(data_s)))
    ax2.set_yscale('log')
    ax2.pcolormesh(times, freqs, numpy.log(data_s), norm=scaleZ, cmap='jet')
    ax2.plot(crs.time_converter(fund_polygon.exterior.xy[0]).datetime, fund_polygon.exterior.xy[1], 'k')
    ax2.plot(crs.time_converter(harm_polygon.exterior.xy[0]).datetime, harm_polygon.exterior.xy[1], '--b')
    ax2.set_ylim((100, 1500))

    plt.show()


def load_catalogue(tfcat_file=TFCAT_FILE):
    return TFCat.from_file(tfcat_file)


def plot_all(tfcat_file=TFCAT_FILE):
    """plot quicklooks for all features in catalogue

    :param tfcat_file: Input TFCat file
    :type tfcat_file: Path
    """
    cat = load_catalogue(tfcat_file)
    for event in cat.iter_features:
        plot_event(event, cat.crs)


if __name__ == "__main__":
    plot_all(TFCAT_FILE)
