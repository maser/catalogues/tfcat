#!/usr/bin/env python3
# -*- encode: utf-8 -*-

from matplotlib import pyplot as plt
from scipy.io import loadmat
from astropy.time import Time
from astropy.units import Unit
from pathlib import Path
from tfcat import Feature, MultiPolygon, Polygon, CRS, FeatureCollection, dump
from tfcat.validate import validate_file
from jsonschema.exceptions import ValidationError

MATLAB_REF_TIME = Time('0000-01-01 00:00:00') - 1 * Unit('day')
MATLAB_DATA_FILE = Path('SKRHarm_contours_v0505.mat')
TABLE_DATA_FILE = Path('wu_2022_table_a1.txt')
FIELDS_META_FILE = Path('fields.json')
CRS_META_FILE = Path('crs.json')
PROPERTIES_META_FILE = Path('properties.json')
TFCAT_FILE = Path('wu_2022_table_a1.json')
SATURN_RADIUS_KM = 60268 * Unit('km')
SCHEMA_URI = "https://voparis-ns.obspm.fr/maser/tfcat/v1.0/tfcat-v1.0-rc2.json"
DOI = "10.25935/t033-qs72"


def validate_coords(coords):
    """Validate coordinate using shapely.

    Validation includes:
    - Polygon must be CCW (to select the inner portion of the coordinate plane);
    - Polygon must be closed (first coordinate point must be equal to last coordinate point);
    - Polygon must not cross itself.

    Mitigation methods:
    - if list of coordinate points are not CCW, reverse the ordering
    - if polygon is not closed, close it
    - if polygon crosses itself, split into two polygons, and keep th longest one

    :param coords: coordinate list to be validated
    :return: validated (and possibly updated) coordinate list
    """

    from shapely.geometry import LinearRing, MultiPolygon, GeometryCollection, Polygon
    from shapely.validation import make_valid

    # check if CCW
    if not LinearRing(coords).is_ccw:
        coords = coords[::-1]

    # check if Polygon is valid (closed and not crossing-over)
    if not Polygon(coords).is_valid:
        # running make_valid => output may be of several types
        res = make_valid(Polygon(coords))
        if isinstance(res, MultiPolygon):
            # Case 1: MultiPolygon => select the longest one
            mp_lengths = [len(item.boundary.coords.xy[0]) for item in res.geoms]
            sp = res.geoms[mp_lengths.index(max(mp_lengths))]
        elif isinstance(res, GeometryCollection):
            # Case 2: GeometryCollection => filter to select Polygons and select only the longest one
            sp = [item for item in res.geoms if isinstance(item, Polygon)]
            mp_lengths = [len(item.exterior.coords.xy[0]) for item in sp]
            sp = sp[mp_lengths.index(max(mp_lengths))]
        elif isinstance(res, Polygon):
            # Case 3: this is already a Polygon => ok
            sp = res

        if not sp.is_valid:
            raise AssertionError('Polygon Still invalid...')
        # Rebuild coords
        x, y = sp.exterior.coords.xy
        coords = [(t, f) for t, f in zip(x, y)]

    return coords


def load_contour_lines(raw_time, raw_freq):
    """Load the contours from the MatLab file, validate the contours and store into TFCat Polygons.

    The times are converted into UNIX timestamps, and frequencies into kHz.

    :param raw_time: list of times
    :param raw_freq: list of frequencies
    :return: List of TFCat Polygon objects
    """
    events = []
    for cur_raw_time, cur_raw_freq in zip(raw_time, raw_freq):
        coords = list(
            map(
                lambda x: ((MATLAB_REF_TIME + x[0] * Unit('day')).unix, (float(x[1]) * Unit('Hz')).to('kHz').value),
                [(t, f) for t, f in zip(cur_raw_time[:, 0], cur_raw_freq[:, 0])]
            )
        )
        coords = validate_coords(coords)
        events.append(Polygon([coords]))
    return events


def _decode_table_row(row):
    """Internal method to decode the Table A.1

    :param row: tuple with all elements of the row
    :return: converted tuple
    """
    return ((
        int(row[0]),  # id
        Time(f'{row[1].replace("/", "-")} {row[2]}:00').unix,  # start_time
        Time(f'{row[3].replace("/", "-")} {row[4]}:00').unix,  # end_time
        int(row[5]),  # duration (minutes)
        float(row[6]),  # Observer's latitude (deg)
        float(row[7]),  # Observer's Local Time (hr)
        float(row[8])*SATURN_RADIUS_KM.value,  # Observer's Distance (km)
        row[9].strip(),  # Mode of Fundamental emission
        row[10].strip(),  # Mode of Harmonic emission
    ) +
        tuple(float(item) for item in row[11].split('±')) +  # Fitted frequency and stdev for Fundamental emission
        tuple(float(item) for item in row[12].split('±')) +  # Fitted frequency and stdev for Harmonic emission
        (" ".join(row[13:]) if len(row) > 13 else "",)  # comment
    )


def load_table(data_file=TABLE_DATA_FILE):
    """Load data from Table A.1

    :param data_file: Path to text file containing Table A.1 data
    :return: List of tuple with decoded table content
    """
    with open(data_file, 'r') as f:
        raw_table = f.readlines()[3:]
    rows = []
    for row in raw_table:
        decoded = _decode_table_row(row.strip().split())
        rows.append(decoded)
    return rows


def load_crs_meta(json_file=CRS_META_FILE):
    """Load CRS metadata from `crs.json`

    :param json_file: Path to `crs.json``
    :return: dict containing CRS definition metadata
    """
    return _load_json_meta(json_file)


def load_fields_meta(json_file=FIELDS_META_FILE):
    """Load Fields metadata from `fields.json`

    :param json_file: Path to `fields.json`
    :return: dict containing Fields definition metadata
    """
    return _load_json_meta(json_file)


def load_properties_meta(json_file=PROPERTIES_META_FILE):
    """Load Fields metadata from `properties.json`

    :param json_file: Path to `properties.json`
    :return: dict containing Fields properties metadata
    """
    return _load_json_meta(json_file)


def _load_json_meta(json_file=FIELDS_META_FILE):
    """Internal method to load JSON file

    :param json_file: file
    :return: dict
    """
    import json
    with open(json_file, 'r') as f:
        return json.load(f)


def collection():
    """Build the TFCat collection for this catalogue

    :return: FeatureCollection object
    """

    # load raw data from Matlab File
    raw_data = loadmat(MATLAB_DATA_FILE)
    # extract Fundamental emission Polygons
    l_fund = load_contour_lines(raw_data['Fund_time'][0], raw_data['Fund_freq'][0])
    # extract Harmonic emission Polygons
    l_harm = load_contour_lines(raw_data['Harm_time'][0], raw_data['Harm_freq'][0])
    # load metadata table A.1 from txt file
    metadata = load_table()

    # prepare collection
    events = []
    fields_meta = load_fields_meta()
    crs_meta = load_crs_meta()
    properties_meta = load_properties_meta()
    properties_meta['doi'] = f'https://doi.org/{DOI}'

    key_meta = (
        "id",
        "time_start",
        "time_end",
        "duration",
        "latitude",
        "local_time",
        "distance",
        "mode_fundamental",
        "mode_harmonic",
        "frequency_fit_fundamental",
        "frequency_sigma_fundamental",
        "frequency_fit_harmonic",
        "frequency_sigma_harmonic",
        "comment"
    )
    if set(key_meta) != set(fields_meta.keys()):
        raise KeyError("Inconsistent fields and properties keys")

    # Loop on items to build Features.
    for cur_meta, cur_fund, cur_harm in zip(metadata, l_fund, l_harm):
        events.append(Feature(
            id=cur_meta[0],
            geometry=MultiPolygon([
                # NB: geometries are MultiPolygon types: 1st Polygon is the Fundamental
                # emission, and the second is the Harmonic emission.
                cur_fund.coordinates,
                cur_harm.coordinates,
            ]),
            properties=dict(zip(key_meta, cur_meta))
        ))

    # Returns a FeatureCollection object
    return FeatureCollection(
        schema=SCHEMA_URI,
        features=events,
        properties=properties_meta,
        fields=fields_meta,
        crs=CRS(crs_meta),
    )


def bbox():
    from tfcat import TFCat
    cat = TFCat.from_file(TFCAT_FILE)

    bbox = None
    for feat in cat.iter_features:
        if bbox is None:
            bbox = list(feat.bbox)
        else:
            if feat.tmin < bbox[0]:
                bbox[0] = feat.tmin
            if feat.fmin < bbox[1]:
                bbox[1] = feat.fmin
            if feat.tmax > bbox[2]:
                bbox[2] = feat.tmax
            if feat.fmax > bbox[3]:
                bbox[3] = feat.fmax
    bbox[0] = cat.crs.time_converter(bbox[0]).isot
    bbox[1] = cat.crs.spectral_converter(bbox[1])
    bbox[2] = cat.crs.time_converter(bbox[2]).isot
    bbox[3] = cat.crs.spectral_converter(bbox[3])

    return bbox


if __name__ == "__main__":
    # open TFCat output file and writes the collection into it:
    with open(TFCAT_FILE, 'w') as f:
        dump(collection(), f)

    try:
        validate_file(TFCAT_FILE, SCHEMA_URI)
    except ValidationError as e:
        raise e
    else:
        print(f'scp {TFCAT_FILE} vmmaser_rpws:/volumes/kronos/doi/{DOI}/content/data')