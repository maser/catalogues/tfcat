#!python3

import json

MOONS = {
    "IO": "Io",
    "EU": "Europa",
    "GA": "Ganymede",
    "CA": "Callisto",
    "AM": "Amalthea",
}

EVENTS = {
    "TMER": {
        "info": "local meridian transit time of Jupiter",
        "ucd": "time.crossing;obs.transit",
    },
    "TBEG": {
        "info": "start of emission",
        "ucd": "time.start",
    },
    "TEND": {
        "info": "end of emission",
        "ucd": "time.end",
    },
    "T_MIN": {
        "info": "time of lowest observed frequency",
        "ucd": "time.epoch;em.freq;stat.min",
    },
    "T_MAX": {
        "info": "time of highest observed frequency",
        "ucd": "time.epoch;em.freq;stat.max",
    },
    "TAV": {
        "info": "average time",
        "ucd": "time.epoch;stat.mean",
    },
    "TAV_W": {
        "info": "weighted average time",
        "ucd": "time.epoch;stat.mean",
    },
}

PARAMS = {
    "CML": {
        "info": "Observer's Central Meridian Longitude",
        "unit": "min",
        "datatype": "int",
        "ucd": "pos.bodyrc.lon"
    },
    "DE": {
        "info": "Observer's Declination",
        "unit": "deg",
        "datatype": "float",
        "ucd": "pos.bodyrc.lat;time.crossing;obs.transit"
    },
    "R": {
        "info": "Observer's Distance",
        "unit": "au",
        "datatype": "float",
        "ucd": "pos.distance;time.crossing;obs.transit"
    },
}

COMPOSITE_PARAMS = {
    "L": {
        "info": "Longitude",
        "unit": "deg",
        "datatype": "float",
        "ucd": "pos.bodyrc.lon"
    },
    "LAT": {
        "info": "Latitude",
        "unit": "deg",
        "datatype": "float",
        "ucd": "pos.bodyrc.lat"
    },
    "PH": {
        "info": "Phase",
        "unit": "deg",
        "datatype": "float",
        "ucd": "pos.angDistance"
    },
}


def create_fields(filename='em_fields.json'):
    params = {
      "pol": {
        "info": "Polarization sense",
        "values": {
          "RH": "Right-Handed",
          "LH": "Left-Handed"
        },
        "datatype": "str",
        "ucd": "meta.code.class;phys.polarization",
      },
      "type": {
        "info": "Radio Source type",
        "datatype": "str",
        "ucd": "meta.code.class",
      },
      "avg_intensity": {
        "info": "Average intensity in emission polygon",
        "unit": "0.3125 dB",
        "datatype": "float",
        "ucd": "spec.line.intensity",
      },
      "avg_intensity_opposite_polarization": {
        "info": "Average intensity in symmetrical emission polygon in opposite polarization",
        "unit": "0.3125 dB",
        "datatype": "float",
        "ucd": "spec.line.intensity",
      },
      "fracpt": {
        "info": "Fraction of points > background in dominant polarization",
        "unit": "%",
        "datatype": "float",
        "ucd": "meta.number",
      },
      "polc": {
        "info": "Circular polarization ratio (LH>0, RH<0)",
        "unit": "%",
        "datatype": "float",
        "ucd": "phys.polarization.circular",
      },
      "obs_id": {
        "info": "ID of corresponding observation",
        "datatype": "int",
        "ucd": "meta.id",
      },
      "npt": {
        "info": "Number of summits of emission polygon",
        "datatype": "int",
        "ucd": "meta.number",
      },
      "time_start": {
        "info": "record time start (minutes of day)",
        "datatype": "int",
        "unit": "min",
        "ucd": "time.epoch;stat.min",
      },
      "time_end": {
        "info": "record time end (minutes of day)",
        "datatype": "int",
        "unit": "min",
        "ucd": "time.epoch;stat.max",
      },
      "time_meridian": {
        "info": "meridian transit time (minutes of day)",
        "datatype": "int",
        "unit": "min",
        "ucd": "time.epoch;obs.transit",
      },
      "freq_min": {
        "info": "record freq min (MHz)",
        "datatype": "float",
        "unit": "MHz",
        "ucd": "em.freq;stat.min",
      },
      "freq_max": {
        "info": "record freq max (MHz)",
        "datatype": "float",
        "unit": "MHz",
        "ucd": "em.freq;stat.max",
      }
    }

    for ke, ve in EVENTS.items():
        for kp, vp in PARAMS.items():
            key = f'{kp}_{ke}'
            value = {
                "info": f'{vp["info"]} at {ve["info"]}',
                "datatype": vp["datatype"],
                "unit": vp["unit"],
                "ucd": f'{vp["ucd"]};{ve["ucd"]}'
            }
            params[key] = value
        for km, vm in MOONS.items():
            for kc, vc in COMPOSITE_PARAMS.items():
                if kc == 'LAT':
                    key = f'{km}{kc}_{ke}'
                else:
                    key = f'{kc}{km}_{ke}'
                value = {
                    "info": f'{vc["info"]} of {vm} at {ve["info"]}',
                    "datatype": vc["datatype"],
                    "unit": vc["unit"],
                    "ucd": f'{vc["ucd"]};{ve["ucd"]}'
                }
                params[key] = value

    with open(filename, 'w') as f:
        json.dump(params, f)


if __name__ == "__main__":
    create_fields()
