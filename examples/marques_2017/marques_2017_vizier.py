#!/usr/bin/env python
"""
This script produces TFCat files from the Marques et al. 2017 catalogue, as stored in
Vizier, under reference J/A+A/604/A17
"""

__author__ = 'Baptiste Cecconi'
__copyright__ = 'Copyright 2021, Baptiste Cecconi, PADC/MASER'
__credits__ = ['Baptiste Cecconi']
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = 'Baptiste Cecconi'
__email__ = 'baptiste.cecconi@obspm.fr'
__status__ = "development"

from astroquery.vizier import Vizier
from tfcat import TFCat, Feature, Polygon, FeatureCollection
from astropy.time import Time
from astropy.units import Unit

VIZIER_CATALOGUE_ID = 'J/A+A/604/A17'
VIZIER_ROW_LIMIT = 15000


def bbox(str_obs_date, str_time_start, str_time_end, freq_min, freq_max):
    return (
        Time(f'{str_obs_date} {str_time_start}'),
        Time(f'{str_obs_date} {str_time_end}'),
        freq_min * Unit('MHz'),
        freq_max * Unit('MHz')
    )


def vizier_obs_to_feature(row):
    """
    Provides a TFCat.Feature from a row in the Vizier `Obs` Catalogue

    :param row: Row fro the `Obs` catalogue
    :type row: dict
    :return: A TFCat Feature built with the input information
    :rtype: Feature
    """

    # Extracting bbox (e.g., tmin/tmax/fmin/fmax)
    tmin, tmax, fmin, fmax = \
        bbox(row["Obs.date"], row["Obs.timeS"], row["Obs.timeE"], row['FreqMin'], row['FreqMax'])

    # Writing Feature: bbox as geometry and dictionary
    return Feature(
        id=row['RankObs'],
        geometry=Polygon([
            (tmin.unix, fmin.value),
            (tmax.unix, fmin.value),
            (tmax.unix, fmax.value),
            (tmin.unix, fmax.value),
            (tmin.unix, fmin.value)
        ]),
        properties=dict(row)
    )


# Load `Obs` table
marques_obs_table = Vizier(
    catalog=f'{VIZIER_CATALOGUE_ID}/obs',
    row_limit=VIZIER_ROW_LIMIT
).query_constraints()[0]


def em_pol_value(val):
    """
    Returns the string-coded polarization for the input numerical coding:
    - `RH` for 0
    - `LH` for 1

    :param val: integer code for polarization in Marques et al 2017 tables
    :type val: int
    :return: decoded polarization
    :rtype: str
    """
    return {0: "RH", 1: "LH"}[int(val)]


def em_type_value(val):
    """
    Returns the string-coded radio emissions name for the input numerical coding:
    - 0: `Io-A`
    - 1: `Io-B`
    - 2: `Io-A'`
    - 3: `Io-A''`
    - 4: `Io-C`
    - 5: `Io-D`
    - 6: `non-Io-A`
    - 7: `non-Io-B`
    - 9: `non-Io-C`
    - 10: `non-Io-D`
    - 11: `undefined`
    - 12: `Io-B`

    :param val: integer code for emission type in Marques et al 2017 tables
    :type val: int
    :return: decoded emission type
    :rtype: str
    """
    return {
        0: "Io-A",
        1: "Io-B",
        2: "Io-A'",
        3: "Io-A''",
        4: "Io-C",
        5: "Io-D",
        6: "non-Io-A",
        7: "non-Io-B",
        9: "non-Io-C",
        10: "non-Io-D",
        11: "undefined",
        12: "Io-B'"}[int(val)]


def obs_from_rank(rank):
    """
    Get `Obs` row from `Em` RankObs

    /!\  The RankObs is incorrect in `Em` table! Add 1 to get the correct record

    :param rank: RankObs value
    :type rank: int
    :return: a row of the `Obs` catalogue
    :rtype: VizierClass
    """
    return Vizier(
        catalog=f'{VIZIER_CATALOGUE_ID}/obs', row_limit=1
    ).query_constraints(RankObs=rank+1)[0]


def vizier_em_to_feature(row):
    """
    Provides a TFCat.Feature from a row in the Vizier `Em` Catalogue

    :param row: Row fro the `Em` catalogue
    :type row: dict
    :return: A TFCat Feature built with the input information
    :rtype: Feature
    """

    # Getting corresponding `Obs` row.
    #
    obs = obs_from_rank(row['RankObs'])

    # Compute start time of observation
    tstart = Time(f"{obs['Obs.date'][0]} {obs['Obs.timeS'][0]}")

    # Return a Feature, with a Polygon and a dictionary in properties
    return Feature(
        id=int(row['Rank']),
        geometry=Polygon(
            [(
                (tstart + row[f'Time{i+1}'] * Unit('s')).unix,  # unixtime of Point
                row[f'Freq{i+1}']*30/400+10 # frequency (MHz) of Point
            )
            for i in range(row['Npt'])]
        ),
        properties={
            'pol': em_pol_value(row['Pol']),
            'type': em_type_value(row['Type']),
            'aip': float(row['AIP']),
            'ais': float(row['AIS']),
            'fracpt': float(row['fracpt']),
            'polc': float(row['PolC']),
            'obs_id': int(row['RankObs']),
            'npt': int(row['Npt']),
            'time_start': row['TimeS'],
            'time_end': row['TimeE'],
            'freq_min': row['FreqMin'],
            'freq_max': row['FreqMax'],
        },
    )


em_table = Vizier(
    catalog=f'{VIZIER_CATALOGUE_ID}/em',
    row_limit=100
).query_constraints()[0]

em_properties = {
    "instrument_host_name": "Nançay Decameter Array",
    "instrument_name": "Routine",
    "title": "Catalogue of Jupiter decametric radio emissions over 26 years",
    "authors": [{
        "GivenName": "Manilo S.",
        "FamilyName": "Marques",
        "ORCID": "https://orcid.org/0000-0003-4896-7301",
        "Affiliation": "https://ror.org/04wn09761"
    }, {
        "GivenName": "Philippe",
        "FamilyName": "Zarka",
        "ORCID": "https://orcid.org/0000-0003-1672-9878",
        "Affiliation": "https://ror.org/02eptjh02"
    }, {
        "GivenName": "Ezequiel",
        "FamilyName": "Echer",
        "ORCID": "https://orcid.org/0000-0002-8351-6779",
        "Affiliation": "https://ror.org/04xbn6x09"
    }, {
        "GivenName": "Vladimir",
        "FamilyName": "Ryabov",
        "ORCID": "https://orcid.org/0000-0002-9243-7597",
        "Affiliation": "https://ror.org/05szw2z23"
    }],
    "bib_reference": "2017A&A...604A..17M",
    "vizier_catalog_id": "J/A+A/604/A17",
    "doi": "10.25935/xxxx-xxxx",
    "publisher": "PADC/MASER",
    "version": "1.0"
}
em_crs = {
    "name": "Time-Frequency",
    "properties": {
        "time_coords": {
            "id": "unix",
            "name": "Timestamp (Unix Time)",
            "unit": "s",
            "time_origin": "1970-01-01T00:00:00.000Z",
            "time_scale": "TT"
        },
        "spectral_coords": {
            "name": "Frequency",
            "unit": "MHz"
        },
        "ref_position": {
            "id": "GEOCENTER"
        }
    }
}

em_fields = {
    'pol': {
        "info": "Polarization sense",
        "values": {
            "RH": "Right-Handed",
            "LH": "Left-Handed"
        },
        "datatype": "str",
        "ucd": "phys.polarization"
    },
    'type': {
        "info": "Radio Source type",
        "datatype": "str",
        "ucd": "meta.code.class"
    },
    'aip': {
        "info": "Average intensity in emission polygon",
        "unit": "0.3125 dB",
        "datatype": "float",
        "ucd": "spec.line.intensity"
    },
    'ais': {
        "info": "Average intensity in symmetrical emission polygon in opposite polarization",
        "unit": "0.3125 dB",
        "datatype": "float",
        "ucd": "spec.line.intensity"
    },
    'fracpt': {
        "info": "Fraction of points > background in dominant polarization",
        "unit": "%",
        "datatype": "float",
        "ucd": "meta.number"
    },
    'polc': {
        "info": "Circular polarization ratio (LH>0, RH<0)",
        "unit": "%",
        "datatype": "float",
        "ucd": "phys.polarization.circular"
    },
    'obs_id': {
        "info": "ID of corresponding observation",
        "datatype": "int",
        "ucd": "meta.id"
    },
    'npt': {
        "info": "Number of summits of emission polygon",
        "datatype": "int",
        "ucd": "meta.number"
    },
    'time_start': {
        'info': "record time start (h:m:s)",
        'datatype': 'str'
    },
    'time_end': {
        'info': "record time end (h:m:s)",
        'datatype': 'str'
    },
    'freq_min': {
        'info': "record freq min (MHz)",
        'datatype': 'str'
    },
    'freq_max': {
        'info': "record freq max (MHz)",
        'datatype': 'str'
    }
}

em_events = map(vizier_em_to_feature, em_table)
em_tfcat = TFCat(FeatureCollection(
    features=list(em_events),
    properties=em_properties,
    crs=em_crs,
    fields=em_fields
))


def plot_em(i):
    from matplotlib import pyplot as plt

    feat = em_tfcat._data.as_table()['feature'][i]['coordinates']
    obs = obs_from_rank(feat[i]['properties']['obs_id'])
    tmin, tmax, fmin, fmax = \
        bbox(obs["Obs.date"], obs["Obs.timeS"], obs["Obs.timeE"], obs['FreqMin'], obs['FreqMax'])
    tt = []
    ff = []
    for item in feat:
        tt.append(Time(item[0], format='unix').datetime)
        ff.append(item[1])
    plt.plot(tt,ff, '+-')

    plt.plot([tmin.datetime, tmax.datetime, tmax.datetime, tmin.datetime, tmin.datetime],
             [fmin.value, fmin.value, fmax.value, fmax.value, fmin.value])

    plt.savefig('test.png')


if __name__ == "__main__":
    ...
