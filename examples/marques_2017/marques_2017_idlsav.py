#!/usr/bin/env python
"""
This script produces TFCat files from the Marques et al. 2017 catalogue, as stored in
the IDL Saveset file provided by P. Zarka
"""

__author__ = 'Baptiste Cecconi'
__copyright__ = 'Copyright 2021, Baptiste Cecconi, PADC/MASER'
__credits__ = ['Baptiste Cecconi']
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = 'Baptiste Cecconi'
__email__ = 'baptiste.cecconi@obspm.fr'
__status__ = "development"

from scipy.io import readsav
from tfcat import Feature, Polygon, FeatureCollection, CRS, dump
from tfcat.validate import validate_file
from astropy.time import Time
from astropy.units import Unit
import datetime
import numpy
from shapely.geometry import LinearRing
import json
from pathlib import Path
from jsonschema.exceptions import ValidationError

MARQUES_IDLSAV = Path("/Users/baptiste/Projets/VOParis/MASER/Catalogues/Catalogue_Marques_v13/new_catalog.sav")
SCHEMA_URI = "https://voparis-ns.obspm.fr/maser/tfcat/v1.0/tfcat-v1.0-rc2.json"


def em_pol_value(val):
    """
    Returns the string-coded polarization for the input numerical coding:
    - `RH` for 0
    - `LH` for 1

    :param val: integer code for polarization in Marques et al 2017 tables
    :type val: int
    :return: decoded polarization
    :rtype: str
    """
    return {0: "RH", 1: "LH"}[int(val)]


def em_type_value(val):
    """
    Returns the string-coded radio emissions name for the input numerical coding:
    - 0: `undefined`
    - 1: `Io-A`
    - 2: `Io-A'`
    - 3: `Io-A''`
    - 4: `Io-B`
    - 5: `Io-B'`
    - 6: `Io-C`
    - 7: `Io-D`
    - 8: `non-Io-A`
    - 9: `non-Io-B`
    - 10: `non-Io-C`
    - 11: `non-Io-D`
    NB: this list is different from the Vizier published version !

    :param val: integer code for emission type in Marques et al 2017 tables
    :type val: int
    :return: decoded emission type
    :rtype: str
    """
    return {
        0: "undefined",
        1: "Io-A",
        2: "Io-A'",
        3: "Io-A''",
        4: "Io-B",
        5: "Io-B'",
        6: "Io-C",
        7: "Io-D",
        8: "non-Io-A",
        9: "non-Io-B",
        10: "non-Io-C",
        11: "non-Io-D",
    }[int(val)]


def load_marques_em_idlsav(file_name=MARQUES_IDLSAV):
    idlsav_data = readsav(file_name)
    return idlsav_data.emg, idlsav_data.emeph


def _load_json(json_file):
    with open(json_file, 'r') as f:
        json_data = json.load(f)
    return json_data


def load_properties():
    return _load_json('em_properties.json')


def load_fields():
    filename = Path('em_fields.json')
    if not filename.exists():
        from fields import create_fields
        create_fields(filename)
    return _load_json(filename)


def load_crs():
    return _load_json('em_crs.json')


def fix_json_value(value):
    if isinstance(value, (numpy.int32, numpy.int16, numpy.int8, numpy.uint8)):
        return int(value)
    elif value is numpy.ma.masked:
        return None
    elif isinstance(value, numpy.float32):
        return float(value)
    else:
        return value


print(f'Loading data from {MARQUES_IDLSAV}')
emg, emeph = load_marques_em_idlsav()

emeph_keys = emeph.dtype.names

em_properties = load_properties()
em_fields = load_fields()
crs = CRS(load_crs())

commands = []

id_offset = 0
for year in range(1990, 2020):
    print(f'Preparing catalogues for emissions in {year}')
    events = []
    obslist = []
    cur_obs_id = None
    ww = (year*10000<emg['ymd']) & (emg['ymd']<(year+1)*10000)
    for id_emi, (cur_emg, cur_emeph) in enumerate(zip(emg[ww], emeph[ww])):
        tt = cur_emg['time'][0:cur_emg['npts']]
        f0 = cur_emg['fmin'][0:cur_emg['npts']]
        f1 = cur_emg['fmax'][0:cur_emg['npts']]
        cur_date = Time(datetime.datetime.strptime(str(cur_emg['ymd']), "%Y%m%d"))

        coords = list(
            map(
                lambda x: ((cur_date + x[0]*Unit('min')).unix, float(x[1])),
                [(t, f) for t, f in zip(tt, f0)] + [(t, f) for t, f in zip(tt, f1)][::-1]
            )
        )

        if not LinearRing(coords).is_ccw:
            coords = coords[::-1]

        event_properties = {
            'pol': fix_json_value(cur_emg.pol),
            'type': em_type_value(cur_emg.source),
            'avg_intensity': fix_json_value(cur_emg.iav),
            'avg_intensity_opposite_polarization': fix_json_value(cur_emg.iav_sym),
            'fracpt': fix_json_value(cur_emg.percent),
            'polc': fix_json_value(cur_emg.polar),
            'obs_id': fix_json_value(cur_emg.nobs),
            'npt': fix_json_value(cur_emg.npts),
            'time_start': fix_json_value(cur_emg.tbeg),
            'time_end': fix_json_value(cur_emg.tend),
            'f_min': fix_json_value(cur_emg.f_min),
            'f_max': fix_json_value(cur_emg.f_max),
        }
        for key in emeph_keys:
            if not isinstance(cur_emeph[key], numpy.ndarray):
                event_properties[key] = float(cur_emeph[key])
            else:
                event_properties[key] = [float(item) for item in cur_emeph[key][0:cur_emg['npts']]]
        event_properties['TIME'] = [float(item) for item in tt]

        events.append(Feature(
            id=id_emi + id_offset,
            geometry=Polygon([coords]),
            properties=event_properties,
        ))

    emg_cat = FeatureCollection(
        schema=SCHEMA_URI,
        features=events,
        properties=em_properties,
        fields=em_fields,
        crs=crs
    )
    if len(events) > 0:
        tfcat_name = f'marques2017_V13_new_{year}.json'
        print(f'Writing catalogue into {tfcat_name}')
        with open(tfcat_name, 'w') as f:
            dump(emg_cat, f)
        try:
            validate_file(tfcat_name, SCHEMA_URI)
        except ValidationError as e:
            raise e
        else:
            print(f'> {tfcat_name} is valid ({SCHEMA_URI})')

        commands.append(
            f'scp marques2017_V13_new_{year}.json vmmaser_rpws:/volumes/kronos/doi/10.25935/ytmj-wz43/content'
        )
    else:
        print(f'No Data for {year}')

    id_offset += len(events)

print('\n')
for command in commands:
    print(command)
