#!/usr/bin/env python3
# -*- encode: utf-8 -*-

import numpy
import datetime
from matplotlib import pyplot as plt
from scipy.io import loadmat
from astropy.time import Time
from astropy.units import Unit
from pathlib import Path
from tfcat import Feature, LineString, CRS, FeatureCollection, dump
from tfcat.validate import validate_file
from shapely.geometry import shape
from shapely.ops import unary_union

ALL = ["load_txt_list", "contour_lines"]
SCHEMA_URI = "https://voparis-ns.obspm.fr/maser/tfcat/v1.0/tfcat-v1.0-rc2.json"
DOI = "10.25935/zmmz-ca38"


matlab_ref_time = Time('0000-01-01 00:00:00') - 1 * Unit('day')


def date_convert(yy, jj, hh, mm):
    """
    converts Year-day Hour:minutes into datetime
    :param yy: year (4 digits)
    :param jj: day of year
    :param hh: hour
    :param mm: minutes
    :return:
    """
    date_format = "%Y-%j %H:%M"
    return Time(datetime.datetime.strptime(f"{yy}-{jj:03d} {hh:02d}:{mm:02d}", date_format))


def loader_txt_catalogue(csv_file: Path or str, verbose=False) -> numpy.array:
    """
    Loads the catalogue from CSV file
    :param csv_file: file name
    :param verbose: verbose flag (default to False)
    :return: catalogue
    """
    # prepare output data types
    dt = numpy.dtype([
        ('id', int),
        ('start', Time),
        ('end', Time),
        ('mode', int)
    ])

    # read file
    with open(csv_file, 'r') as f:
        raw_data = f.readlines()

    # split items in each row
    data = [tuple(map(int, item.strip().split(','))) for item in raw_data[1:]]

    # mapping into final format
    final = [(x[0], date_convert(*x[1:5]), date_convert(*x[5:9]), x[9]) for x in data]

    # returning numpy.ndarray
    return numpy.array(final, dtype=dt)


def converter_txt_catalogue(raw: numpy.array, freq) -> FeatureCollection:
    """
    converts the txt catalogue into a FeatureCollection object (with LineString geometries)
    :param raw: raw data dervied from `loader_txt_catalogue`
    :param freq: reference frequency of narrow band event (value should be 20 or 5, units in kHz)
    :return:
    """

    if freq not in (5, 20):
        raise ValueError("freq keyword should be either 5 or 20 (kHz)")

    events = []
    for item in raw:
        events.append(Feature(
            id=int(item[0]),
            geometry=LineString([(item[1].unix, freq), (item[2].unix, freq)]),
            properties={
                'mode': int(item[3])
            }
        ))

    # define the global properties
    properties = {
        "instrument_host_name": "Cassini",
        "instrument_name": "RPWS",
        "receiver_name": "HFR",
        "title": "Statistical Study of Spatial Distribution and Polarization for Saturn Narrowband Emissions. "
                 "Supplementary material: Event catalogue",
        "authors": [{
            "GivenName": "Si Yuan",
            "FamilyName": "Wu",
            "ORCID": "https://orcid.org/0000-0002-1326-8829",
            "Affiliation": "https://ror.org/049tv2d57"
        }, {
            "GivenName": "Shengyi",
            "FamilyName": "Ye",
            "ORCID": "https://orcid.org/0000-0002-3064-1082",
            "Affiliation": "https://ror.org/049tv2d57"
        }, {
            "GivenName": "Goeg",
            "FamilyName": "Fischer",
            "ORCID": "https://orcid.org/0000-0002-0431-2381",
            "Affiliation": "https://ror.org/002w0jp71"
        }, {
            "GivenName": "Wang",
            "FamilyName": "Jian",
            "Affiliation": "https://ror.org/049tv2d57",
            "ORCID": "https://orcid.org/0000-0002-7381-4713"
        }, {
            "GivenName": "Minyi",
            "FamilyName": "Long",
            "Affiliation": "https://ror.org/033vjfk17",
            "ORCID": "https://orcid.org/0000-0001-6984-5640"
        }, {
            "GivenName": "J. Douglas",
            "FamilyName": "Menietti",
            "Affiliation": "https://ror.org/036jqmy94",
            "ORCID": "https://orcid.org/0000-0001-6737-251X"
        }, {
            "GivenName": "Baptiste",
            "FamilyName": "Cecconi",
            "Affiliation": "https://ror.org/029nkcm90",
            "ORCID": "https://orcid.org/0000-0001-7915-5571"
        }, {
            "GivenName": "William",
            "FamilyName": "Kurth",
            "Affiliation": "https://ror.org/036jqmy94",
            "ORCID": "https://orcid.org/0000-0002-5471-6202"
        }],
        "bib_reference": "https://doi.org/10.3847/1538-4357/ac0af1",
        "doi": f"https://doi.org/{DOI}",
        "data_source_reference": ["https://doi.org/10.25935/5jfx-dh49"],
        "publisher": "MASER/PADC",
        "version": "1.0",
        "target_name": "Saturn",
        "target_class": "planet",
        "target_region": "magnetosphere",
        "feature_name": "Saturn narrow band emissions"
        # TODO: add spectral_range_min, spectral_range_max, time_min, time_max (using shapely?)
    }

    # define fields
    fields = {
        "mode": {
            "info": "Radio wave propagation mode (0=LO, 2=Z)",
            "datatype": "int",
            "ucd": "meta.code.class;em.radio"
        }
    }

    # define Coordinate Reference System
    crs = CRS({
        "type": "local",
        "properties": {
            "name": "Time-Frequency",
            "time_coords_id": "unix",
            "spectral_coords": {
                "type": "frequency",
                "unit": "kHz"
            },
            "ref_position_id": "cassini-orbiter"
        }
    })

    collection = FeatureCollection(
        schema=SCHEMA_URI,
        features=events,
        properties=properties,
        fields=fields,
        crs=crs
    )
    return collection


def writer_txt_catalogue(file_name, collection: FeatureCollection):
    with open(file_name, 'w') as f:
        dump(collection, f)


def load_contour_lines(mat_file: Path or str, verbose=False) -> numpy.array:
    """
    Loads contour_lines MatLab file
    :param mat_file: MatLab file
    :param verbose: default to False
    :return:
    """
    raw = loadmat(mat_file)

    raw_times = matlab_ref_time + raw['c21'][0] * Unit('day')
    raw_freqs = raw['c21'][1] * Unit('kHz')
    raw_dtimes = raw_times[1:] - raw_times[:-1]
    return raw_times, raw_freqs, raw_dtimes


def split_contour_lines(raw: numpy.array, limit=None, verbose=False) -> numpy.array:
    #catalogue: numpy.array,

    """
    Computes the contour lines from the raw processing output
    (this code is derived from `getcontourlines.m` by Siyuan Wu)

    :param raw: input raw contour
    :param limit: optional limit on samples to be loaded
    :param verbose: verbose flag
    :return: contour array
    """

    raw_times, raw_freqs, raw_dtimes = raw

    s = []
    new_feature = True
    prev_positive_dt = True

    for t, f, dt in zip(raw_times, raw_freqs, raw_dtimes):
        # a feature is composed of a back and forth path in time.

        # check if new feature starts: previous dt is negative and current is positive
        positive_dt = dt >= 0
        if positive_dt and not prev_positive_dt:
            new_feature = True
        prev_positive_dt = positive_dt
        if new_feature:
            if verbose:
                if len(s) > 0:
                    print(s[-1]['t'][0], len(s), len(s[-1]['t']))
            s.append({'t': [], 'f': [], 'dt': []})
            new_feature = False

        s[-1]['t'].append(t)
        s[-1]['f'].append(f)
        s[-1]['dt'].append(dt)

    return s

#    for icat, feature in enumerate(catalogue):
#        s.append(
#            dict(list(zip(catalogue.dtype.names, feature)) + [('t', []), ('f', [])])
#        )
#        cur_start = s[icat]['start']
#        cur_end = s[icat]['end']
#        while True:
#            t, f = next(raw_times), next(raw_freqs)
#            if (cur_start - 1 * Unit('min')) <= t < cur_start:
#                s[icat]['start'] = cur_start - 1 * Unit('min')
#            if cur_end <= t < (cur_end + 1 * Unit('min')):
#                s[icat]['end'] = cur_end + 1 * Unit('min')
#            if cur_start <= t <= (s[icat]['end'] + 1 * Unit('min')):
#                s[icat]['t'].append(t)
#                s[icat]['f'].append(f)
#            else:
#                break

#    return numpy.array(s)


if __name__ == "__main__":

    import sys

    args = sys.argv[1:]
    if args[0] == '-c':
        file_name = Path(args[1])
        freqs = {
            'NB5_list.txt': 5,
            'NB20_list.txt': 20
        }
        print(f'Processing file: {file_name}')
        raw_txt_cat = loader_txt_catalogue(file_name)
        collection = converter_txt_catalogue(raw_txt_cat, freqs[file_name.name])
        tfcat_file_name = file_name.parent / f"{file_name.stem}.json"
        writer_txt_catalogue(tfcat_file_name, collection)
        validate_file(tfcat_file_name, SCHEMA_URI)
        print(f'scp {tfcat_file_name} vmmaser_rpws:/volumes/kronos/doi/{DOI}/content')

    else:
        raw_txt_cat = loader_txt_catalogue('NB20_list.txt')

        l = load_contour_lines('c20_NB_contour_lines.mat')
        #s = split_contour_lines(l)

        day_start = numpy.min(raw_txt_cat['start']).datetime.date()
        day_end = numpy.max(raw_txt_cat['end']).datetime.date()
        ndays = (day_end - day_start).days

        t, f, dt = l

        for day in range(ndays):
            cur_day = day_start + datetime.timedelta(days=day)
            cur_day_start = Time(cur_day.isoformat())
            cur_day_end = cur_day_start + 1 * Unit('d')
            mask_l = (cur_day_start <= t) & (t <= cur_day_end)
            mask_c = (cur_day_start <= raw_txt_cat['end']) & (raw_txt_cat['start'] <= cur_day_end)
            if any(mask_l) or any(mask_c):
                fig = plt.figure()
                if any(mask_l):
                    plt.plot(
                        [item.datetime for item in t[mask_l]],
                        [item.value for item in f[mask_l]],
                        '+'
                    )
                if any(mask_c):
                    for cc in raw_txt_cat[mask_c]:
                        plt.plot([cc['start'].datetime, cc['start'].datetime], [10,30], '--k')
                        plt.plot([cc['start'].datetime, cc['end'].datetime], [20,20], '-k')
                        plt.plot([cc['end'].datetime, cc['end'].datetime], [10, 30], '--k')
                plt.xlim((cur_day_start.datetime, cur_day_end.datetime))
                plt.title(cur_day.isoformat())
                plt.savefig(f'daily-quicklooks/{cur_day.isoformat()}.png')
                plt.close(fig)

#    for ss, cc in zip(s,c):
#        plt.plot((cc['start'].datetime, cc['end'].datetime), (20, 20))
#        plt.plot([item.datetime for item in ss['t']], [item.value for item in ss['f']],',')
#        plt.show()

