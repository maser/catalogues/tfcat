;==================================================================================
pro thp_file_read, name, dat

;-- old version of thp-files
; str3 = strarr(3)
; openr, lun, name, /GET_LUN, /COMPRESS
; readf, lun, str3
; a = EXECUTE(str3(2))
; readu, lun, dat
; free_lun, lun
;dat = CREATE_STRUCT('probe', str3(0), 'coord', str3(1), dat)


 str2 = strarr(2)
 openr, lun, name, /GET_LUN, /COMPRESS
 readf, lun, str2
 a = EXECUTE(str2[0]) ;-- ... creates 'dat_str'
 readf, lun, dat_str
 a = EXECUTE(str2[1]) ;-- ... creates 'dat'
 readu, lun, dat
 free_lun, lun

 dat = CREATE_STRUCT(dat_str, dat)

end
;==================================================================================