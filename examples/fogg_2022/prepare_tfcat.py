#!/usr/bin/env python3
# -*- encode: utf-8 -*-

from pathlib import Path
import csv
from astropy.time import Time
from astropy.units import Unit
from tfcat import Feature, Polygon, CRS, FeatureCollection, dump
from shapely.geometry import shape
from shapely.ops import unary_union

INPUT_FILE = Path('fogg_akr_burst_list_2000_2004.csv')
SCHEMA_URI = "https://voparis-ns.obspm.fr/maser/tfcat/v1.0/tfcat-v1.0-rc2.json"


def reader(csv_file: Path or str) -> dict:
    """
    Reads and yields raw data from the original CSV file
    :param csv_file: input file
    :return: row dictionary
    """

    # open up the CSV file
    with open(INPUT_FILE) as f:
        rows = csv.DictReader(f, delimiter=',', quotechar='"')
        seq = 0
        for row in rows:
            yield {
                "seq": seq,
                "start_time": row['stime'],
                "end_time": row['etime'],
                "burst_timestamp": [Time(item.strip())
                                     for item in row['burst_timestamp'].split(',')
                                     if item.strip() != 'NaT'],
                "min_f_bound": [float(item.strip())*Unit("kHz")
                                for item in row['min_f_bound'].split(',')
                                if item.strip() != 'nan'],
                "max_f_bound": [float(item.strip())*Unit("kHz")
                                for item in row['max_f_bound'].split(',')
                                if item.strip() != 'nan']
            }
            seq += 1


def event_converter(data_source):
    """
    Converts the input data into a TFCat Feature

    :param data_source: iterator yielding row dictionaries (see reader function)
    :return: Feature
    """

    for input_data in data_source:
        tt = [item.unix for item in input_data['burst_timestamp']]
        fmin = [item.value for item in input_data['min_f_bound']]
        fmax = [item.value for item in input_data['max_f_bound']]
        contour = [list(zip(tt, fmin)) + list(zip(tt, fmax))[::-1] + [(tt[0], fmin[0])]]
        yield Feature(
            id=input_data['seq'],
            geometry=Polygon(contour)
        )


def converter(input_file: Path, yearly=True):
    """
    Converts the input data into a TFCat Feature Collection

    :param input_file: file name (CSV) to read the data from
    :param yearly: flag for yearly ouput
    :return: dictionary with FeatureCollection values
    """

    print(f"Processing file: {input_file}")
    # define the global properties
    properties = {
        "instrument_host_name": "wind",
        "instrument_name": "waves",
        "receiver_name": "rad1",
        "target_name": "Earth",
        "target_class": "planet",
        "target_region": "magnetosphere",
        "feature_name": "Radio emissions#AKR#Auroral Kilometric Radiation",
        "title": "Bursts of Auroral Kilometric Radiation individually selected from Wind/WAVES data",
        "authors": [{
            "GivenName": "Alexandra",
            "FamilyName": "Fogg",
            "ORCID": "https://orcid.org/0000-0002-1139-5920",
            "Affiliation": "https://ror.org/051sx6d27"
        }, {
            "GivenName": "Caitriona",
            "FamilyName": "Jackman",
            "ORCID": "https://orcid.org/0000-0003-0635-7361",
            "Affiliation": "https://ror.org/051sx6d27"
        }, {
            "GivenName": "James E.",
            "FamilyName": "Waters",
            "ORCID": "https://orcid.org/0000-0001-8164-5414",
            "Affiliation": "https://ror.org/01ryk1543"
        }, {
            "GivenName": "Xavier",
            "FamilyName": "Bonnin",
            "ORCID": "https://orcid.org/0000-0003-4217-7333",
            "Affiliation": "https://ror.org/02eptjh02"
        }, {
            "GivenName": "Laurent",
            "FamilyName": "Lamy",
            "ORCID": "https://orcid.org/0000-0002-8428-1369",
            "Affiliation": ["https://ror.org/02eptjh02", "https://ror.org/00ssy9q55"]
        }, {
            "GivenName": "Baptiste",
            "FamilyName": "Cecconi",
            "ORCID": "https://orcid.org/0000-0001-7915-5571",
            "Affiliation": "https://ror.org/02eptjh02"
        }, {
            "GivenName": "Karine",
            "FamilyName": "Issautier",
            "ORCID": "https://orcid.org/0000-0002-2757-101X",
            "Affiliation": "https://ror.org/02eptjh02"
        }, {
            "GivenName": "Corentin K.",
            "FamilyName": "Louis",
            "ORCID": "https://orcid.org/0000-0002-9552-8822",
            "Affiliation": "https://ror.org/02eptjh02"
        }],
        "bib_reference": "https://doi.org/10.1029/2021JA030209",
        "doi": "https://doi.org/10.25935/hfjx-xx26",
        "publisher": "PADC",
        "version": "1.0",
    }

    # define fields
    fields = {}

    # define Coordinate Reference System
    crs = CRS({
        "type": "local",
        "properties": {
            "name": "Time-Frequency",
            "time_coords_id": "unix",
            "spectral_coords": {
                "type": "frequency",
                "unit": "kHz"
            },
            "ref_position_id": "wind",
        }
    })

    # Fill in a FeatureCollection object
    features = {}
    for event in event_converter(reader(input_file)):
        if yearly:
            cur_year = Time(event.tmin, format='unix').to_datetime().year
        else:
            cur_year = 'full'
        if cur_year not in features.keys():
            features[cur_year] = []
        features[cur_year].append(event)

    collections = {}
    for year in features.keys():
        bbox = unary_union([shape(event.geometry) for event in features[year]]).bounds
        properties['time_min'] = crs.time_converter(bbox[0]).isot
        properties['spectral_range_min'] = crs.spectral_converter(bbox[1]).to('Hz').value
        properties['time_max'] = crs.time_converter(bbox[2]).isot
        properties['spectral_range_max'] = crs.spectral_converter(bbox[3]).to('Hz').value
        collections[year] = FeatureCollection(
            schema=SCHEMA_URI,
            features=features[year],
            properties=properties,
            fields=fields,
            crs=crs
        )

    return collections


def writer(input_file, yearly=True):
    # writes out a TFCat JSON file
    if yearly:
        files = [
            ('fogg_akr_burst_list_2000.json', 2000),
            ('fogg_akr_burst_list_2001.json', 2001),
            ('fogg_akr_burst_list_2002.json', 2002),
            ('fogg_akr_burst_list_2003.json', 2003),
            ('fogg_akr_burst_list_2004.json', 2004),
        ]
    else:
        files = [
            ('fogg_akr_burst_list_2000_2004.json', 'full')
        ]

    collections = converter(input_file, yearly=yearly)

    for cur_file, year in files:
        print(f"Writing file: {cur_file}")
        with open(cur_file, 'w') as f:
            dump(collections[year], f)
        print(f"Validating file: {cur_file}")
        validate(cur_file)


def validate(file_name):
    from tfcat.validate import validate_file
    validate_file(file_name, schema_uri="https://voparis-ns.obspm.fr/maser/tfcat/v1.0/tfcat-v1.0-rc2.json")


if __name__ == "__main__":
    writer(INPUT_FILE, yearly=True)
