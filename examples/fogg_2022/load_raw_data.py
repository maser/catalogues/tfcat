#!/usr/bin/env python3
# -*- encode: utf-8 -*-

import requests

RAW_DATA_URL = "https://maser.obspm.fr/doi/10.25935/hfjx-xx26/content/fogg_akr_burst_list_2000_2004.csv"

file_name = RAW_DATA_URL.split('/')[-1]

r = requests.get(RAW_DATA_URL, allow_redirects=True)
open(file_name, 'wb').write(r.content)
