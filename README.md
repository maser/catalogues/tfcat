#  Time-Frequency Catalogue (TFCat) Python Module

This module implements an interface for loading TFCat JSON 
data files. 

TFCat JSON Specification: [TFcat](https://gitlab.obspm.fr/maser/catalogues/catalogue-format)

TFCat JSON Schema: https://voparis-ns.obspm.fr/maser/tfcat/v1.0/schema#

*This version 1.0.0*

## Installation

TFCat is available from PyPI: https://pypi.org/project/TFCat/

```
pip install TFCat
```


## Acknowlegments
- This TFCat library has been derived from the 
  [jazzband/geojson](https://github.com/jazzband/geojson) python library.
